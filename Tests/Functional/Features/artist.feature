Feature:
  Tests related to artistController
  Scenario: get artists' list
    Given fixtures are reloaded
    And authenticated
    When guest sends request to "/api/v1/artists"
    Then the response has code 200 and json:
    """
    {
      "0":{
        "id":"0660083a-3546-4143-ae74-c73bc06209e7",
        "first_name":"firstName",
        "last_name":"lastName",
        "nick":"nick",
        "_links":{
           "self":{
              "href":"/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e7"
           },
           "songs":{
              "href":"/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e7/songs"
           }
        }
      },
      "1":{
        "id":"0660083a-3546-4143-ae74-c73bc06209e5",
        "first_name":"firstName",
        "last_name":"lastName",
        "nick":"nick",
        "_links":{
           "self":{
              "href":"/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e5"
           },
           "songs":{
              "href":"/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e5/songs"
           }
        }
      },
      "2":{
        "id":"3f9da869-a230-4fc6-8012-40a5f06d694e",
        "first_name":"second art",
        "last_name":"last sec",
        "nick":"next nicxknick",
        "_links":{
           "self":{
              "href":"/api/v1/artist/3f9da869-a230-4fc6-8012-40a5f06d694e"
           },
           "songs":{
              "href":"/api/v1/artist/3f9da869-a230-4fc6-8012-40a5f06d694e/songs"
           }
        }
      },
      "3":{
        "id":"d9741ab8-784e-45bf-af92-0416e97730da",
        "first_name":"to delete",
        "last_name":"to delete",
        "nick":"to delete",
        "_links":{
           "self":{
              "href":"/api/v1/artist/d9741ab8-784e-45bf-af92-0416e97730da"
           },
           "songs":{
              "href":"/api/v1/artist/d9741ab8-784e-45bf-af92-0416e97730da/songs"
           }
        }
      },
      "4":{
        "id":"d9741ab8-784e-45bf-af92-0416e97730dasdx",
        "first_name":"nex t f ",
        "last_name":" next t l",
        "nick":" next  t n",
        "_links":{
           "self":{
              "href":"/api/v1/artist/d9741ab8-784e-45bf-af92-0416e97730dasdx"
           },
           "songs":{
              "href":"/api/v1/artist/d9741ab8-784e-45bf-af92-0416e97730dasdx/songs"
           }
        }
      }
    }
    """

  Scenario: get specific artist
    Given authenticated
    When guest sends request to "/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e7"
    Then the response has code 200 and json:
    """
    {
        "id": "0660083a-3546-4143-ae74-c73bc06209e7",
        "first_name": "firstName",
        "last_name": "lastName",
        "nick": "nick",
        "_links": {
            "self": {
                "href": "/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e7"
            },
            "artist_songs": {
                "href": "/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e7/songs"
            }
        }
    }
    """

  Scenario: error when artist doesn't exists
    Given authenticated
    When guest sends request to "/api/v1/artist/1"
    Then the response has code 404


  Scenario: add artist
    Given authenticated
    When guest sends save request to "/api/v1/artist" with json:
    """
    {
      "first_name": "Some name",
      "last_name": "Some last name",
      "nick": "My nick"
    }
    """
    Then the response should be created with id

  Scenario: add artist with invalid data
    Given authenticated
    When guest sends save request to "/api/v1/artist" with json:
    """
    {
      "first_name": "",
      "last_name": "",
      "nick": "",
      "songs_ids": {
      	"0": "11423848-458d-4d4b-beeb-7317b703d2212" ,"asd": "0660083a-3546-4143-ae74-c73bc06209e3"
      }
    }
    """
    Then the response should contain validation errors with json:
    """
    {
      "first_name": [
          {
              "field": "first_name",
              "message": "This value should not be blank."
          }
      ],
      "last_name": [
          {
              "field": "last_name",
              "message": "This value should not be blank."
          }
      ],
      "nick": [
          {
              "field": "nick",
              "message": "This value should not be blank."
          }
      ],
      "songs_ids": [
          {
              "field": "songs_ids",
              "message": "Selected fields doesn't exists: 11423848-458d-4d4b-beeb-7317b703d2212"
          }
      ]
    }
    """

  Scenario: edit artist
    Given authenticated
    When guest sends update request to "/api/v1/artist/3f9da869-a230-4fc6-8012-40a5f06d694e" with json:
    """
    {
      "first_name": "new name",
      "last_name": "new last name",
      "nick": "New nick"
    }
    """
    Then the response should be created with id
    When guest sends request to "/api/v1/artist/3f9da869-a230-4fc6-8012-40a5f06d694e"
    Then the response has code 200 and json:
    """
    {
        "id": "3f9da869-a230-4fc6-8012-40a5f06d694e",
        "first_name": "new name",
        "last_name": "new last name",
        "nick": "New nick",
        "_links": {
            "self": {
                "href": "/api/v1/artist/3f9da869-a230-4fc6-8012-40a5f06d694e"
            },
            "artist_songs": {
                "href": "/api/v1/artist/3f9da869-a230-4fc6-8012-40a5f06d694e/songs"
            }
        }
    }
    """

  Scenario: edit with incorrect data artist
    Given authenticated
    When guest sends update request to "/api/v1/artist/3f9da869-a230-4fc6-8012-40a5f06d694e" with json:
    """
    {
      "first_name": "",
      "last_name": "",
      "nick": "",
      "songs_ids": {
      	"0": "11423848-458d-4d4b-beeb-7317b703d2212" ,"asd": "0660083a-3546-4143-ae74-c73bc06209e3"
      }
    }
    """
    Then the response should contain validation errors with json:
    """
    {
      "first_name": [
          {
              "field": "first_name",
              "message": "This value should not be blank."
          }
      ],
      "last_name": [
          {
              "field": "last_name",
              "message": "This value should not be blank."
          }
      ],
      "nick": [
          {
              "field": "nick",
              "message": "This value should not be blank."
          }
      ],
      "songs_ids": [
          {
              "field": "songs_ids",
              "message": "Selected fields doesn't exists: 11423848-458d-4d4b-beeb-7317b703d2212"
          }
      ]
    }
    """

  Scenario: delete artist
    Given authenticated
    When guest sends delete request to "/api/v1/artist/d9741ab8-784e-45bf-af92-0416e97730da"
    Then the response has code 201
    When guest sends request to "/api/v1/artist/d9741ab8-784e-45bf-af92-0416e97730da"
    Then the response has code 404


  Scenario: get artist's songs
    Given authenticated
    When guest sends request to "/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e7/songs"
    Then the response has code 200 and json:
    """
    {
       "id":"0660083a-3546-4143-ae74-c73bc06209e7",
       "songs":[
          {
             "id":"0660083a-3546-4143-ae74-c73bc06209e2",
             "title":"other title1",
             "genre":"genre2",
             "ismn":"ismn3",
             "year":1994,
             "_links":{
                "api_get_song":{
                   "href":"/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e2"
                }
             }
          },
          {
             "id":"0660083a-3546-4143-ae74-c73bc06209e3",
             "title":"other title2",
             "genre":"genre2",
             "ismn":"ismn3",
             "year":1994,
             "_links":{
                "api_get_song":{
                   "href":"/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e3"
                }
             }
          },
          {
             "id":"11423848-458d-4d4b-beeb-7317b703d212",
             "title":"some title",
             "genre":"genre",
             "ismn":"ismn",
             "year":1994,
             "_links":{
                "api_get_song":{
                   "href":"/api/v1/song/11423848-458d-4d4b-beeb-7317b703d212"
                }
             }
          }
       ],
       "_links":{
          "self":{
             "href":"/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e7/songs"
          }
       }
    }
    """

  Scenario: edit artist with song
    Given authenticated
    When guest sends update request to "/api/v1/artist/3f9da869-a230-4fc6-8012-40a5f06d694e" with json:
    """
    {
      "first_name": "new name 2",
      "last_name": "new last name 2",
      "nick": "New nick",
      "songs_ids": {
      	"0": "11423848-458d-4d4b-beeb-7317b703d212" ,"asd": "0660083a-3546-4143-ae74-c73bc06209e3"
      }
    }
    """
    Then the response should be created with id
    When guest sends request to "/api/v1/artist/3f9da869-a230-4fc6-8012-40a5f06d694e"
    Then the response has code 200 and json:
    """
    {
      "id": "3f9da869-a230-4fc6-8012-40a5f06d694e",
      "first_name": "new name 2",
      "last_name": "new last name 2",
      "nick": "New nick",
      "_links": {
          "self": {
              "href": "/api/v1/artist/3f9da869-a230-4fc6-8012-40a5f06d694e"
          },
          "artist_songs": {
              "href": "/api/v1/artist/3f9da869-a230-4fc6-8012-40a5f06d694e/songs"
          }
      }
    }
    """
