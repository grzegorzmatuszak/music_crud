Feature:
  Tests related to songController

  Scenario: get songs' list
    Given fixtures are reloaded
    And authenticated
    When guest sends request to "/api/v1/songs"
    Then the response has code 200 and json:
    """
    {
      "0": {
          "id": "11423848-458d-4d4b-beeb-7317b703d212",
          "title": "some title",
          "genre": "genre",
          "ismn": "ismn",
          "year": 1994,
          "_links": {
              "self": {
                  "href": "/api/v1/song/11423848-458d-4d4b-beeb-7317b703d212"
              },
              "artists": {
                  "href": "/api/v1/song/11423848-458d-4d4b-beeb-7317b703d212/artists"
              }
          }
      },
      "1": {
          "id": "0660083a-3546-4143-ae74-c73bc06209e1",
          "title": "other title",
          "genre": "genre2",
          "ismn": "ismn3",
          "year": 1994,
          "_links": {
              "self": {
                  "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e1"
              },
              "artists": {
                  "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e1/artists"
              }
          }
      },
      "2": {
          "id": "0660083a-3546-4143-ae74-c73bc06209e2",
          "title": "other title1",
          "genre": "genre2",
          "ismn": "ismn3",
          "year": 1994,
          "_links": {
              "self": {
                  "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e2"
              },
              "artists": {
                  "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e2/artists"
              }
          }
      },
      "3": {
          "id": "0660083a-3546-4143-ae74-c73bc06209e3",
          "title": "other title2",
          "genre": "genre2",
          "ismn": "ismn3",
          "year": 1994,
          "_links": {
              "self": {
                  "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e3"
              },
              "artists": {
                  "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e3/artists"
              }
          }
      },
      "4": {
          "id": "0660083a-3546-4143-ae74-c73bc06209e4",
          "title": "other title3",
          "genre": "genre2",
          "ismn": "ismn3",
          "year": 1994,
          "_links": {
              "self": {
                  "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e4"
              },
              "artists": {
                  "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e4/artists"
              }
          }
      },
      "5": {
          "id": "0660083a-3546-4143-ae74-13212451",
          "title": "without artist",
          "genre": "genre2",
          "ismn": "ismn3",
          "year": 1994,
          "_links": {
              "self": {
                  "href": "/api/v1/song/0660083a-3546-4143-ae74-13212451"
              },
              "artists": {
                  "href": "/api/v1/song/0660083a-3546-4143-ae74-13212451/artists"
              }
          }
      }
   }
    """

  Scenario: get specific song
    Given authenticated
    When guest sends request to "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e1"
    Then the response has code 200 and json:
    """
     {
        "title": "other title",
        "id": "0660083a-3546-4143-ae74-c73bc06209e1",
        "genre": "genre2",
        "ismn": "ismn3",
        "year": 1994,
        "_links": {
            "self": {
                "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e1"
            },
            "song_artists": {
                "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e1/artists"
            },
            "publisher": {
                "href": "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e1"
            }
        },
        "_embedded": {
            "song_artists": {
                "0":{
                    "id": "d9741ab8-784e-45bf-af92-0416e97730da",
                    "first_name": "to delete",
                    "last_name": "to delete",
                    "nick": "to delete",
                    "_links": {
                        "self": {
                            "href": "/api/v1/artist/d9741ab8-784e-45bf-af92-0416e97730da"
                        },
                        "artist_songs": {
                            "href": "/api/v1/artist/d9741ab8-784e-45bf-af92-0416e97730da/songs"
                        }
                    }
                },
                "1":{
                    "id": "0660083a-3546-4143-ae74-c73bc06209e5",
                    "first_name": "firstName",
                    "last_name": "lastName",
                    "nick": "nick",
                    "_links": {
                        "self": {
                            "href": "/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e5"
                        },
                        "artist_songs": {
                            "href": "/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e5/songs"
                        }
                    }
                }
            },
            "publisher": {
                "id": "0660083a-3546-4143-ae74-c73bc06209e1",
                "name": "sone publi",
                "nip": "nip2",
                "address": "addressess ",
                "_links": {
                    "self": {
                        "href": "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e1"
                    },
                    "publisher_songs": {
                        "href": "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e1/songs"
                    }
                }
            }
        }
     }
    """

  Scenario: error when song doesn't exists
    Given authenticated
    When guest sends request to "/api/v1/song/1"
    Then the response has code 404


  Scenario: add song
    Given authenticated
    When guest sends save request to "/api/v1/song" with json:
    """
    {
      "title": "Some name",
      "genre": "Some  genre",
      "ismn": "My ismn",
      "year":1994,
      "publisher_id":"2660083a-3546-4143-ae74-c73bc06209e2",
      "artists_ids": {
      	"0": "d9741ab8-784e-45bf-af92-0416e97730da"
      }
    }
    """
    Then the response should be created with id

  Scenario: add song with invalid data
    Given authenticated
    When guest sends save request to "/api/v1/song" with json:
    """
    {
      "title": "",
      "genre": "",
      "ismn": "",
      "year":"",
      "publisher_id": "1142384dfdsfsdfsdfsdffsddfsa dsfsdf sdf8-458d-4d4b-beeb-7317b703asdasdasdd2212",
      "artists_ids": []
    }
    """
    Then the response should contain validation errors with json:
    """
    {
      "title": [
          {
              "field": "title",
              "message": "This value should not be blank."
          }
      ],
      "genre": [
          {
              "field": "genre",
              "message": "This value should not be blank."
          }
      ],
      "ismn": [
          {
              "field": "ismn",
              "message": "This value should not be blank."
          }
      ],
      "year": [
          {
             "field":"year",
             "message":"This value should have exactly 4 characters."
          },
          {
             "field":"year",
             "message":"This value should be positive."
          }
      ],
      "publisher_id": [
          {
              "field": "publisher_id",
              "message": "Selected fields doesn't exists: 1142384dfdsfsdfsdfsdffsddfsa dsfsdf sdf8-458d-4d4b-beeb-7317b703asdasdasdd2212"
          }
      ],
      "artists_ids": [
          {
              "field": "artists_ids",
              "message": "This value should not be blank."
          }
      ]
    }
    """

  Scenario: edit song
    Given authenticated
    When guest sends update request to "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e4" with json:
    """
    {
      "title": "new name",
      "genre": "new genre",
      "ismn": "New ismn",
      "year":1990,
      "publisher_id":"2660083a-3546-4143-ae74-c73bc06209e2",
      "artists_ids": {
      	"0": "d9741ab8-784e-45bf-af92-0416e97730dasdx"
      }
    }
    """
    Then the response should be created with id
    When guest sends request to "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e4"
    Then the response has code 200 and json:
    """
    {
        "title": "new name",
        "id": "0660083a-3546-4143-ae74-c73bc06209e4",
        "genre": "new genre",
        "ismn": "New ismn",
        "year": 1990,
        "_links": {
            "self": {
                "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e4"
            },
            "song_artists": {
                "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e4/artists"
            },
            "publisher": {
                "href": "/api/v1/publisher/2660083a-3546-4143-ae74-c73bc06209e2"
            }
        },
        "_embedded": {
            "song_artists": {
                "0":{
                    "id": "d9741ab8-784e-45bf-af92-0416e97730dasdx",
                    "first_name": "nex t f ",
                    "last_name": " next t l",
                    "nick": " next  t n",
                    "_links": {
                        "self": {
                            "href": "/api/v1/artist/d9741ab8-784e-45bf-af92-0416e97730dasdx"
                        },
                        "artist_songs": {
                            "href": "/api/v1/artist/d9741ab8-784e-45bf-af92-0416e97730dasdx/songs"
                        }
                    }
                }
            },
            "publisher": {
                "id": "2660083a-3546-4143-ae74-c73bc06209e2",
                "name": " publi",
                "nip": "nip2",
                "address": "addressess ",
                "_links": {
                    "self": {
                        "href": "/api/v1/publisher/2660083a-3546-4143-ae74-c73bc06209e2"
                    },
                    "publisher_songs": {
                        "href": "/api/v1/publisher/2660083a-3546-4143-ae74-c73bc06209e2/songs"
                    }
                }
            }
        }
    }
    """

  Scenario: edit with incorrect data song
    Given authenticated
    When guest sends update request to "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e4" with json:
    """
    {
      "title": "",
      "genre": "",
      "ismn": "",
      "year":"",
      "publisher_id":"11423848-458d-4d4b-beeb-7317b703d2212",
      "artists_ids": {
      	"0": "d9741ab8-784e-45bf-af92-0416e97730daasdasd"
      }
    }
    """
    Then the response should contain validation errors with json:
    """
    {
      "title": [
          {
              "field": "title",
              "message": "This value should not be blank."
          }
      ],
      "genre": [
          {
              "field": "genre",
              "message": "This value should not be blank."
          }
      ],
      "ismn": [
          {
              "field": "ismn",
              "message": "This value should not be blank."
          }
      ],
      "year": [
           {
              "field": "year",
              "message": "This value should have exactly 4 characters."
          },
           {
              "field": "year",
              "message": "This value should be positive."
          }
      ],
      "publisher_id": [
          {
              "field": "publisher_id",
              "message": "Selected fields doesn't exists: 11423848-458d-4d4b-beeb-7317b703d2212"
          }
      ],
      "artists_ids": [
          {
              "field": "artists_ids",
              "message": "Selected fields doesn't exists: d9741ab8-784e-45bf-af92-0416e97730daasdasd"
          }
      ]
    }
    """

  Scenario: delete song
    Given authenticated
    When guest sends delete request to "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e3"
    Then the response has code 201
    When guest sends request to "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e3"
    Then the response has code 404

  Scenario: get song's artists
    Given authenticated
    When guest sends request to "/api/v1/song/11423848-458d-4d4b-beeb-7317b703d212/artists"
    Then the response has code 200 and json:
    """
    {
        "id": "11423848-458d-4d4b-beeb-7317b703d212",
        "artists": {
            "0": {
                "id": "d9741ab8-784e-45bf-af92-0416e97730da",
                "first_name": "to delete",
                "last_nme": "to delete",
                "nick": "to delete",
                "_links": {
                    "api_get_artist": {
                        "href": "/api/v1/artist/d9741ab8-784e-45bf-af92-0416e97730da"
                    }
                }
            },
            "1": {
                "id": "3f9da869-a230-4fc6-8012-40a5f06d694e",
                "first_name": "second art",
                "last_nme": "last sec",
                "nick": "next nicxknick",
                "_links": {
                    "api_get_artist": {
                        "href": "/api/v1/artist/3f9da869-a230-4fc6-8012-40a5f06d694e"
                    }
                }
            },
            "2": {
                "id": "0660083a-3546-4143-ae74-c73bc06209e5",
                "first_name": "firstName",
                "last_nme": "lastName",
                "nick": "nick",
                "_links": {
                    "api_get_artist": {
                        "href": "/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e5"
                    }
                }
            },
            "3": {
                "id": "0660083a-3546-4143-ae74-c73bc06209e7",
                "first_name": "firstName",
                "last_nme": "lastName",
                "nick": "nick",
                "_links": {
                    "api_get_artist": {
                        "href": "/api/v1/artist/0660083a-3546-4143-ae74-c73bc06209e7"
                    }
                }
            }
        },
        "_links": {
            "self": {
                "href": "/api/v1/song/11423848-458d-4d4b-beeb-7317b703d212/artists"
            }
        }
    }
    """
  Scenario: get song's artists when song doesn't have artists
    Given authenticated
    When guest sends request to "/api/v1/song/0660083a-3546-4143-ae74-13212451/artists"
    Then the response has code 200 and json:
    """
    {
        "id": "0660083a-3546-4143-ae74-13212451",
        "artists": [],
        "_links": {
            "self": {
                "href": "/api/v1/song/0660083a-3546-4143-ae74-13212451/artists"
            }
        }
    }
    """
