Feature:
  Tests related to publisherController


  Scenario: get publishers' list
    Given fixtures are reloaded
    And authenticated
    When guest sends request to "/api/v1/publishers"
    Then the response has code 200 and json:
    """
    {
      "0":{
          "id": "11423848-458d-4d4b-beeb-7317b703d212",
          "name": "sone publiuszer",
          "nip": "nip",
          "address": "address ",
          "_links": {
              "self": {
                  "href": "/api/v1/publisher/11423848-458d-4d4b-beeb-7317b703d212"
              },
              "songs": {
                  "href": "/api/v1/publisher/11423848-458d-4d4b-beeb-7317b703d212/songs"
              }
          }
      },
      "1":{
          "id": "0660083a-3546-4143-ae74-c73bc06209e1",
          "name": "sone publi",
          "nip": "nip2",
          "address": "addressess ",
          "_links": {
              "self": {
                  "href": "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e1"
              },
              "songs": {
                  "href": "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e1/songs"
              }
          }
      },
      "2":{
          "id": "0660083a-3546-4143-ae74-c73bc06209e2",
          "name": "sone publi",
          "nip": "nip2",
          "address": "addressess ",
          "_links": {
              "self": {
                  "href": "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e2"
              },
              "songs": {
                  "href": "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e2/songs"
              }
          }
      },
      "3":{
          "id": "1660083a-3546-4143-ae74-c73bc06209e2",
          "name": "empty song publi",
          "nip": "nip2",
          "address": "addressess ",
          "_links": {
              "self": {
                  "href": "/api/v1/publisher/1660083a-3546-4143-ae74-c73bc06209e2"
              },
              "songs": {
                  "href": "/api/v1/publisher/1660083a-3546-4143-ae74-c73bc06209e2/songs"
              }
          }
      },
      "4":{
          "id": "2660083a-3546-4143-ae74-c73bc06209e2",
          "name": " publi",
          "nip": "nip2",
          "address": "addressess ",
          "_links": {
              "self": {
                  "href": "/api/v1/publisher/2660083a-3546-4143-ae74-c73bc06209e2"
              },
              "songs": {
                  "href": "/api/v1/publisher/2660083a-3546-4143-ae74-c73bc06209e2/songs"
              }
          }
      },
      "5":{
          "id": "3660083a-3546-4143-ae74-c73bc06209e2",
          "name": " remove it",
          "nip": "nip2",
          "address": "addressess ",
          "_links": {
              "self": {
                  "href": "/api/v1/publisher/3660083a-3546-4143-ae74-c73bc06209e2"
              },
              "songs": {
                  "href": "/api/v1/publisher/3660083a-3546-4143-ae74-c73bc06209e2/songs"
              }
          }
      }
    }
    """

  Scenario: get specific publisher
    Given authenticated
    When guest sends request to "/api/v1/publisher/11423848-458d-4d4b-beeb-7317b703d212"
    Then the response has code 200 and json:
    """
    {
      "id": "11423848-458d-4d4b-beeb-7317b703d212",
      "name": "sone publiuszer",
      "nip": "nip",
      "address": "address ",
      "_links": {
          "self": {
              "href": "/api/v1/publisher/11423848-458d-4d4b-beeb-7317b703d212"
          },
          "publisher_songs": {
              "href": "/api/v1/publisher/11423848-458d-4d4b-beeb-7317b703d212/songs"
          }
      }
    }
    """

  Scenario: error when publisher doesn't exists
    Given authenticated
    When guest sends request to "/api/v1/publisher/1"
    Then the response has code 404


  Scenario: add publisher
    Given authenticated
    When guest sends save request to "/api/v1/publisher" with json:
    """
    {
      "name": "some title",
      "nip": "some nip",
      "address": "some address"
    }
    """
    Then the response should be created with id

  Scenario: add publisher with invalid data
    Given authenticated
    When guest sends save request to "/api/v1/publisher" with json:
    """
    {
      "name": "",
      "nip": "",
      "address": "",
      "songs_ids": {
      	"0": "11423848-458d-4d4b-beeb-7317b703d2212" ,"asd": "0660083a-3546-4143-ae74-c73bc06209e3"
      }
    }
    """
    Then the response should contain validation errors with json:
    """
    {
      "name": [
          {
              "field": "name",
              "message": "This value should not be blank."
          }
      ],
      "nip": [
          {
              "field": "nip",
              "message": "This value should not be blank."
          }
      ],
      "address": [
          {
              "field": "address",
              "message": "This value should not be blank."
          }
      ],
      "songs_ids": [
          {
              "field": "songs_ids",
              "message": "Selected fields doesn't exists: 11423848-458d-4d4b-beeb-7317b703d2212"
          }
      ]
    }
    """

  Scenario: edit publisher
    Given authenticated
    When guest sends update request to "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e1" with json:
    """
    {
      "name": "new title",
      "nip": "mew nip",
      "address": "New address"
    }
    """
    Then the response should be created with id
    When guest sends request to "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e1"
    Then the response has code 200 and json:
    """
    {
        "id": "0660083a-3546-4143-ae74-c73bc06209e1",
        "name": "new title",
        "nip": "mew nip",
        "address": "New address",
        "_links": {
            "self": {
                "href": "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e1"
            },
            "publisher_songs": {
                "href": "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e1/songs"
            }
        }
    }
    """

  Scenario: edit with incorrect data publisher
    Given authenticated
    When guest sends update request to "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e1" with json:
    """
    {
      "name": "",
      "nip": "",
      "address": "",
      "songs_ids": {
      	"0": "11423848-458d-4d4b-beeb-7317b703d2212" ,"asd": "0660083a-3546-4143-ae74-c73bc06209e3"
      }
    }
    """
    Then the response should contain validation errors with json:
    """
    {
      "name": [
          {
              "field": "name",
              "message": "This value should not be blank."
          }
      ],
      "nip": [
          {
              "field": "nip",
              "message": "This value should not be blank."
          }
      ],
      "address": [
          {
              "field": "address",
              "message": "This value should not be blank."
          }
      ],
      "songs_ids": [
          {
              "field": "songs_ids",
              "message": "Selected fields doesn't exists: 11423848-458d-4d4b-beeb-7317b703d2212"
          }
      ]
    }
    """

  Scenario: delete publisher
    Given authenticated
    When guest sends delete request to "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e1"
    Then the response has code 201
    When guest sends request to "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e1"
    Then the response has code 404


  Scenario: get publisher's songs
    Given authenticated
    When guest sends request to "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e2/songs"
    Then the response has code 200 and json:
    """
    {
      "id": "0660083a-3546-4143-ae74-c73bc06209e2",
      "songs": [
          {
              "id": "0660083a-3546-4143-ae74-c73bc06209e4",
              "title": "other title3",
              "genre": "genre2",
              "ismn": "ismn3",
              "year": 1994,
              "_links": {
                  "api_get_song": {
                      "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e4"
                  }
              }
          },
          {
              "id": "0660083a-3546-4143-ae74-c73bc06209e3",
              "title": "other title2",
              "genre": "genre2",
              "ismn": "ismn3",
              "year": 1994,
              "_links": {
                  "api_get_song": {
                      "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e3"
                  }
              }
          },
          {
              "id": "0660083a-3546-4143-ae74-c73bc06209e2",
              "title": "other title1",
              "genre": "genre2",
              "ismn": "ismn3",
              "year": 1994,
              "_links": {
                  "api_get_song": {
                      "href": "/api/v1/song/0660083a-3546-4143-ae74-c73bc06209e2"
                  }
              }
          }
      ],
      "_links": {
          "self": {
              "href": "/api/v1/publisher/0660083a-3546-4143-ae74-c73bc06209e2/songs"
          }
      }
  }
    """

  Scenario: edit publisher with song
    Given authenticated
    When guest sends update request to "/api/v1/publisher/2660083a-3546-4143-ae74-c73bc06209e2" with json:
    """
    {
      "name": "new name 2",
      "nip": "new last name 2",
      "address": "New nick",
      "songs_ids": {
      	"0": "11423848-458d-4d4b-beeb-7317b703d212" ,"asd": "0660083a-3546-4143-ae74-c73bc06209e3"
      }
    }
    """
    Then the response should be created with id
    When guest sends request to "/api/v1/publisher/2660083a-3546-4143-ae74-c73bc06209e2"
    Then the response has code 200 and json:
    """
    {
      "id": "2660083a-3546-4143-ae74-c73bc06209e2",
      "name": "new name 2",
      "nip": "new last name 2",
      "address": "New nick",
      "_links": {
          "self": {
              "href": "/api/v1/publisher/2660083a-3546-4143-ae74-c73bc06209e2"
          },
          "publisher_songs": {
              "href": "/api/v1/publisher/2660083a-3546-4143-ae74-c73bc06209e2/songs"
          }
      }
    }
    """
