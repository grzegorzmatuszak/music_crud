<?php
declare(strict_types=1);

namespace Tests\Functional\Context;

use Behat\Behat\Context\Context;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Fidry\AliceDataFixtures\LoaderInterface;

class FixtureContext implements Context
{
    /**
     * @Given fixtures are reloaded
     */
    public function fixturesAreReloaded(): void
    {
        shell_exec('cd '.dirname(__DIR__, 3).' && composer run-script prepare');
    }
}
