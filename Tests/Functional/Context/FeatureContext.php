<?php
declare(strict_types=1);

namespace Tests\Functional\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Coduo\PHPMatcher\Factory\SimpleFactory;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

class FeatureContext implements Context
{
    private KernelInterface $kernel;

    private array $headers = [
        'content-type' => 'application/json',
        'accept' => 'application/json',
        'Accept-Language' => 'en',
    ];

    /**
     * @var Response|null
     */
    private ?Response $response;

    private ?Request $request;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @Given authenticated
     */
    public function guestIsAuthenticated(): void
    {
        if (!$this->headers['authorization']) {
            $this->guestSendsSaveRequestToPathWithData('/api/v1/oauth2/token', '
                {
                  "grant_type": "client_credentials",
                  "client_id": "example_id",
                  "client_secret": "65d4d6fb82d7a65642b71e0deca53722e4fa35d90752a1352f4401d9d6aec8832dd88364a31cf7dcc16973d652edbeec24e6d096f5b8d904ef3f915f1cac40ee"
                }
            ');

            $content = \json_decode($this->response->getContent(), true);
            $this->headers['authorization'] = sprintf('%s %s', $content['token_type'], $content['access_token']);
        }
    }

    /**
     * @When guest sends request to :path
     *
     * @param string $path
     *
     * @throws Exception
     */
    public function guestSendsARequestTo(string $path): void
    {
        $this->response = $this->kernel->handle(Request::create($path, 'GET'));
    }

    /**
     * @When guest sends save request to :path with json:
     *
     * @param string $path
     * @param string $json
     *
     * @throws Exception
     */
    public function guestSendsSaveRequestToPathWithData(string $path, string $json): void
    {
        $this->request = Request::create($path, 'POST', [], [], [], [], $json);
        $this->request->headers->add($this->headers);
        $this->response = $this->kernel->handle($this->request);
    }

    /**
     * @When guest sends update request to :path with json:
     *
     * @param string $path
     * @param string $json
     *
     * @throws Exception
     */
    public function guestSendsUpdateRequestToPathWithData(string $path, string $json): void
    {
        $this->request = Request::create($path, 'PUT', [], [], [], [], $json);
        $this->request->headers->add($this->headers);
        $this->response = $this->kernel->handle($this->request);
    }

    /**
     * @When guest sends delete request to :path
     *
     * @param string $path
     *
     * @throws Exception
     */
    public function guestSendsDeleteRequestToPath(string $path): void
    {
        $this->request = Request::create($path, 'DELETE');
        $this->request->headers->add($this->headers);
        $this->response = $this->kernel->handle($this->request);
    }

    /**
     * @Then the response should be created with id
     */
    public function theResponseShouldBeCreatedWithId(): void
    {
        Assert::assertNotNull($this->response, 'No response received');

        $content = \json_decode($this->response->getContent());
        Assert::assertEquals(Response::HTTP_CREATED, $this->response->getStatusCode());
        Assert::assertIsString($content->id);
    }

    /**
     * @Then the response has code :responseCode and json:
     *
     * @param int          $responseCode
     * @param PyStringNode $json
     */
    public function theResponseHasCodeAndJson(int $responseCode, PyStringNode $json): void
    {
        Assert::assertNotNull($this->response, 'No response received');
        Assert::assertEquals($responseCode, $this->response->getStatusCode());

        $content = \json_decode($this->response->getContent(), true);
        $json = \json_decode($json->getRaw(), true);
        Assert::assertNotNull($json, 'Incorrect json in parameters');
        $factory = new SimpleFactory();
        $matcher = $factory->createMatcher();
        // Use the pattern matcher to verify JSON.
        if (!$matcher->match($content, $json)) {
            Assert::fail(sprintf('JSON mismatch: %s', $matcher->getError()));
        }
    }

    /**
     * @Then the response has code :responseCode
     *
     * @param int $responseCode
     */
    public function theResponseHasCode(int $responseCode): void
    {
        Assert::assertNotNull($this->response, 'No response received');
        Assert::assertEquals($responseCode, $this->response->getStatusCode());
    }

    /**
     * @Then the response should contain validation errors with json:
     *
     * @param PyStringNode $json
     */
    public function theResponseShouldContainValidationErrorsWithJson(PyStringNode $json): void
    {
        Assert::assertNotNull($this->response, 'No response received');
        Assert::assertEquals(Response::HTTP_BAD_REQUEST, $this->response->getStatusCode());
        $content = \json_decode($this->response->getContent(), true);
        $json = \json_decode($json->getRaw(), true);

        Assert::assertEquals($json, $content);
    }
}
