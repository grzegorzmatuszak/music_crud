<?php

namespace App\Tests\Unit\Application\Command\Artist\Delete;

use App\Application\Command\Artist\Delete\DeleteArtistCommand;
use App\Application\Command\Artist\Delete\DeleteArtistCommandHandler;
use App\Domain\Artist\Repository\UpdateArtistRepositoryInterface;
use PHPUnit\Framework\TestCase;

class DeleteArtistCommandHandlerTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenCallExpectsUpdaterCalled(): void
    {
        $updateArtistRepository = $this->createMock(UpdateArtistRepositoryInterface::class);
        $updateArtistRepository->expects($this->once())->method('deleteArtist');

        $message = new DeleteArtistCommand('');
        $handler = new DeleteArtistCommandHandler($updateArtistRepository);
        $handler($message);
    }
}
