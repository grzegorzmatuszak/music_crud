<?php

namespace App\Tests\Unit\Application\Command\Artist\Update;

use App\Application\Command\Artist\Update\UpdateArtistCommand;
use App\Application\Command\Artist\Update\UpdateArtistCommandHandler;
use App\Domain\Artist\Repository\UpdateArtistRepositoryInterface;
use PHPUnit\Framework\TestCase;

class UpdateArtistCommandHandlerTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenCallExpectsUpdaterCalled(): void
    {
        $updateArtistRepository = $this->createMock(UpdateArtistRepositoryInterface::class);
        $updateArtistRepository->expects($this->once())->method('update');

        $message = new UpdateArtistCommand('', '', '', '');
        $handler = new UpdateArtistCommandHandler($updateArtistRepository);
        $handler($message);
    }
}
