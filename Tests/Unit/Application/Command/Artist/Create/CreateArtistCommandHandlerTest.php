<?php

namespace App\Tests\Unit\Application\Command\Artist\Create;

use App\Application\Command\Artist\Create\CreateArtistCommand;
use App\Application\Command\Artist\Create\CreateArtistCommandHandler;
use App\Domain\Artist\Repository\CreateArtistRepositoryInterface;
use PHPUnit\Framework\TestCase;

class CreateArtistCommandHandlerTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenCallExpectsUpdaterCalled(): void
    {
        $artistUpdater = $this->createMock(CreateArtistRepositoryInterface::class);
        $artistUpdater->expects($this->once())->method('save');

        $message = new CreateArtistCommand('', '', '', '', []);
        $handler = new CreateArtistCommandHandler($artistUpdater);
        $handler($message);
    }
}
