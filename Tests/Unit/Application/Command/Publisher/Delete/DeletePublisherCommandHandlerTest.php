<?php

namespace App\Tests\Unit\Application\Command\Publisher\Delete;

use App\Application\Command\Publisher\Delete\DeletePublisherCommand;
use App\Application\Command\Publisher\Delete\DeletePublisherCommandHandler;
use App\Domain\Publisher\Repository\UpdatePublisherRepositoryInterface;
use PHPUnit\Framework\TestCase;

class DeletePublisherCommandHandlerTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenCallExpectsUpdaterCalled(): void
    {
        $updatePublisherRepository = $this->createMock(UpdatePublisherRepositoryInterface::class);
        $updatePublisherRepository->expects($this->once())->method('deletePublisher');

        $message = new DeletePublisherCommand('');
        $handler = new DeletePublisherCommandHandler($updatePublisherRepository);
        $handler($message);
    }
}
