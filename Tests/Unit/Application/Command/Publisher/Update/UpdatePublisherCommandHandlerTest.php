<?php

namespace App\Tests\Unit\Application\Command\Publisher\Update;

use App\Application\Command\Publisher\Update\UpdatePublisherCommand;
use App\Application\Command\Publisher\Update\UpdatePublisherCommandHandler;
use App\Domain\Publisher\Repository\UpdatePublisherRepositoryInterface;
use PHPUnit\Framework\TestCase;

class UpdatePublisherCommandHandlerTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenCallExpectsUpdaterCalled(): void
    {
        $updatePublisherRepository = $this->createMock(UpdatePublisherRepositoryInterface::class);
        $updatePublisherRepository->expects($this->once())->method('update');

        $message = new UpdatePublisherCommand('', '', '', '');
        $handler = new UpdatePublisherCommandHandler($updatePublisherRepository);
        $handler($message);
    }
}
