<?php

namespace App\Tests\Unit\Application\Command\Publisher\Create;

use App\Application\Command\Publisher\Create\CreatePublisherCommand;
use App\Application\Command\Publisher\Create\CreatePublisherCommandHandler;
use App\Domain\Publisher\Repository\CreatePublisherRepositoryInterface;
use PHPUnit\Framework\TestCase;

class CreatePublisherCommandHandlerTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenCallExpectsUpdaterCalled(): void
    {
        $publisherUpdater = $this->createMock(CreatePublisherRepositoryInterface::class);
        $publisherUpdater->expects($this->once())->method('save');

        $message = new CreatePublisherCommand('', '', '', '', []);
        $handler = new CreatePublisherCommandHandler($publisherUpdater);
        $handler($message);
    }
}
