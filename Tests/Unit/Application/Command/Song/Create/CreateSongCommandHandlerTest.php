<?php

namespace App\Tests\Unit\Application\Command\Song\Create;

use App\Application\Command\Song\Create\CreateSongCommand;
use App\Application\Command\Song\Create\CreateSongCommandHandler;
use App\Domain\Song\Repository\CreateSongRepositoryInterface;
use PHPUnit\Framework\TestCase;

class CreateSongCommandHandlerTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenCallExpectsUpdaterCalled(): void
    {
        $songUpdater = $this->createMock(CreateSongRepositoryInterface::class);
        $songUpdater->expects($this->once())->method('save');

        $message = new CreateSongCommand('', '', '', '', 1, '', []);
        $handler = new CreateSongCommandHandler($songUpdater);
        $handler($message);
    }
}
