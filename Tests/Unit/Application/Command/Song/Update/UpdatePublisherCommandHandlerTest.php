<?php

namespace App\Tests\Unit\Application\Command\Song\Update;

use App\Application\Command\Song\Update\UpdateSongCommand;
use App\Application\Command\Song\Update\UpdateSongCommandHandler;
use App\Domain\Song\Repository\UpdateSongRepositoryInterface;
use PHPUnit\Framework\TestCase;

class UpdateSongCommandHandlerTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenCallExpectsUpdaterCalled(): void
    {
        $updateSongRepository = $this->createMock(UpdateSongRepositoryInterface::class);
        $updateSongRepository->expects($this->once())->method('update');

        $message = new UpdateSongCommand('', '', '', '', 1, '', []);
        $handler = new UpdateSongCommandHandler($updateSongRepository);
        $handler($message);
    }
}
