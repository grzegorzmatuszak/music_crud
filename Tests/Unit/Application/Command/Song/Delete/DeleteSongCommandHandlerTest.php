<?php

namespace App\Tests\Unit\Application\Command\Song\Delete;

use App\Application\Command\Song\Delete\DeleteSongCommand;
use App\Application\Command\Song\Delete\DeleteSongCommandHandler;
use App\Domain\Song\Repository\UpdateSongRepositoryInterface;
use PHPUnit\Framework\TestCase;

class DeleteSongCommandHandlerTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenCallExpectsUpdaterCalled(): void
    {
        $updateSongRepository = $this->createMock(UpdateSongRepositoryInterface::class);
        $updateSongRepository->expects($this->once())->method('deleteSong');

        $message = new DeleteSongCommand('');
        $handler = new DeleteSongCommandHandler($updateSongRepository);
        $handler($message);
    }
}
