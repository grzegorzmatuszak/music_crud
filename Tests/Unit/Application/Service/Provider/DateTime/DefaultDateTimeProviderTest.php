<?php

namespace App\Tests\Unit\Application\Service\Provider\DateTime;

use App\Application\Service\Provider\DateTime\DefaultDateTimeProvider;
use PHPUnit\Framework\TestCase;

class DefaultDateTimeProviderTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenAskTwoTimesForDateExpectsTwoTimesTheSameDate(): void
    {
        $defaultDateTimeProvider = new DefaultDateTimeProvider();
        $date1 = $defaultDateTimeProvider->get();
        $date2 = $defaultDateTimeProvider->get();

        $this->assertEquals($date1, $date2);
    }

    /**
     * @test
     */
    public function testWhenAskTwoTimesFirstWasChangedExpectsSecondResultTheSameAsFirst(): void
    {
        $defaultDateTimeProvider = new DefaultDateTimeProvider();
        $date1 = $defaultDateTimeProvider->get();
        $orgDate = clone $date1;

        $date1->setDate(1999, 2, 12);
        $date2 = $defaultDateTimeProvider->get();

        $this->assertEquals($orgDate, $date2);
    }
}
