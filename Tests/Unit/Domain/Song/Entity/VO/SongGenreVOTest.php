<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Song\Entity\VO;

use App\Domain\Song\Entity\VO\SongGenreVO;
use App\Domain\Song\Exception\SongInvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class SongGenreVOTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenTryToCreateWithValidDataExpectsCreated(): void
    {
        $songGenreVO = SongGenreVO::create('test');
        $this->assertEquals('test', $songGenreVO->getGenre());
    }

    /**
     * @test
     * @dataProvider invalidDataProvider
     */
    public function testWhenTryToCreateWithInvalidDataExpectsSongInvalidArgumentException(
        string $genre
    ): void {
        $this->expectException(SongInvalidArgumentException::class);
        SongGenreVO::create($genre);
    }

    public function invalidDataProvider(): array
    {
        return [
            'When genre is too short' => [
                'genre' => '',
            ],
            'When genre is too long' => [
                'genre' => '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901',
            ],
        ];
    }
}
