<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Song\Entity\VO;

use App\Domain\Song\Entity\VO\SongTitleVO;
use App\Domain\Song\Exception\SongInvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class SongTitleVOTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenTryToCreateWithValidDataExpectsCreated(): void
    {
        $songTitleVO = SongTitleVO::create('test');
        $this->assertEquals('test', $songTitleVO->getTitle());
    }

    /**
     * @test
     * @dataProvider invalidDataProvider
     */
    public function testWhenTryToCreateWithInvalidDataExpectsSongInvalidArgumentException(
        string $title
    ): void {
        $this->expectException(SongInvalidArgumentException::class);
        SongTitleVO::create($title);
    }

    public function invalidDataProvider(): array
    {
        return [
            'When title is too short' => [
                'title' => '',
            ],
            'When title is too long' => [
                'title' => '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901',
            ],
        ];
    }
}
