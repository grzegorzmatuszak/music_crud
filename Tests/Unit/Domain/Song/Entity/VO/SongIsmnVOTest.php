<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Song\Entity\VO;

use App\Domain\Song\Entity\VO\SongIsmnVO;
use App\Domain\Song\Exception\SongInvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class SongIsmnVOTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenTryToCreateWithValidDataExpectsCreated(): void
    {
        $songIsmnVO = SongIsmnVO::create('test');
        $this->assertEquals('test', $songIsmnVO->getIsmn());
    }

    /**
     * @test
     * @dataProvider invalidDataProvider
     */
    public function testWhenTryToCreateWithInvalidDataExpectsSongInvalidArgumentException(
        string $ismn
    ): void {
        $this->expectException(SongInvalidArgumentException::class);
        SongIsmnVO::create($ismn);
    }

    public function invalidDataProvider(): array
    {
        return [
            'When ismn is too short' => [
                'ismn' => '',
            ],
            'When ismn is too long' => [
                'ismn' => '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901',
            ],
        ];
    }
}
