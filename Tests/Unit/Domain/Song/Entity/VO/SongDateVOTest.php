<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Song\Entity\VO;

use App\Domain\Song\Entity\VO\SongDateVO;
use App\Domain\Song\Exception\SongInvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class SongDateVOTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenTryToCreateWithValidDataExpectsCreated(): void
    {
        $songDateVO = SongDateVO::create(1234);
        $this->assertEquals(1234, $songDateVO->getYear());
    }

    /**
     * @test
     * @dataProvider invalidDataProvider
     */
    public function testWhenTryToCreateWithInvalidDataExpectsSongInvalidArgumentException(
        int $date
    ): void {
        $this->expectException(SongInvalidArgumentException::class);
        SongDateVO::create($date);
    }

    public function invalidDataProvider(): array
    {
        return [
            'When date is too short' => [
                'date' => 000,
            ],
            'When date is too long' => [
                'date' => 12343,
            ],
        ];
    }
}
