<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Publisher\Entity\VO;

use App\Domain\Publisher\Entity\VO\PublisherNipVO;
use App\Domain\Publisher\Exception\PublisherInvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class PublisherNipVOTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenTryToCreateWithValidDataExpectsCreated(): void
    {
        $publisherNameVO = PublisherNipVO::create('test');
        $this->assertEquals('test', $publisherNameVO->getNip());
    }

    /**
     * @test
     * @dataProvider invalidDataProvider
     */
    public function testWhenTryToCreateWithInvalidDataExpectsPublisherInvalidArgumentException(
        string $nip
    ): void {
        $this->expectException(PublisherInvalidArgumentException::class);
        PublisherNipVO::create($nip);
    }

    public function invalidDataProvider(): array
    {
        return [
            'When nip is too short' => [
                'nip' => '',
            ],
            'When nip is too long' => [
                'nip' => '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901',
            ],
        ];
    }
}
