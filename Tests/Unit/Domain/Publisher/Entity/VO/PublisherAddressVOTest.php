<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Publisher\Entity\VO;

use App\Domain\Publisher\Entity\VO\PublisherAddressVO;
use App\Domain\Publisher\Exception\PublisherInvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class PublisherAddressVOTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenTryToCreateWithValidDataExpectsCreated(): void
    {
        $publisherNameVO = PublisherAddressVO::create('test');
        $this->assertEquals('test', $publisherNameVO->getAddress());
    }

    /**
     * @test
     * @dataProvider invalidDataProvider
     */
    public function testWhenTryToCreateWithInvalidDataExpectsPublisherInvalidArgumentException(
        string $address
    ): void {
        $this->expectException(PublisherInvalidArgumentException::class);
        PublisherAddressVO::create($address);
    }

    public function invalidDataProvider(): array
    {
        return [
            'When address is too short' => [
                'address' => '',
            ],
            'When address is too long' => [
                'address' => '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901',
            ],
        ];
    }
}
