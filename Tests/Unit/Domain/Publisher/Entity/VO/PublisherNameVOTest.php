<?php
declare(strict_types=1);


namespace App\Tests\Unit\Domain\Publisher\Entity\VO;

use App\Domain\Publisher\Entity\VO\PublisherNameVO;
use App\Domain\Publisher\Exception\PublisherInvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class PublisherNameVOTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenTryToCreateWithValidDataExpectsCreated(): void
    {
        $publisherNameVO = PublisherNameVO::create('test');
        $this->assertEquals('test', $publisherNameVO->getName());
    }

    /**
     * @test
     * @dataProvider invalidDataProvider
     */
    public function testWhenTryToCreateWithInvalidDataExpectsPublisherInvalidArgumentException(
        string $name
    ): void {
        $this->expectException(PublisherInvalidArgumentException::class);
        PublisherNameVO::create($name);
    }

    public function invalidDataProvider(): array
    {
        return [
            'When name is too short' => [
                'name' => '',
            ],
            'When name is too long' => [
                'name' => '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901',
            ],
        ];
    }
}
