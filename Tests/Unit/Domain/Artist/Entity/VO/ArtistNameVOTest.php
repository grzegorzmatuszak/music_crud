<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Artist\Entity\VO;

use App\Domain\Artist\Entity\VO\ArtistNameVO;
use App\Domain\Artist\Exception\ArtistInvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class ArtistNameVOTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenTryToCreateWithValidDataExpectsCreated(): void
    {
        $artistNameVO = ArtistNameVO::create('test', 'test');
        $this->assertEquals('test', $artistNameVO->getLastName());
        $this->assertEquals('test', $artistNameVO->getFirstName());
    }

    /**
     * @test
     * @dataProvider invalidDataProvider
     */
    public function testWhenTryToCreateWithInvalidDataExpectsArtistInvalidArgumentException(
        string $firstName,
        string $lastName
    ): void {
        $this->expectException(ArtistInvalidArgumentException::class);
        ArtistNameVO::create($firstName, $lastName);
    }

    public function invalidDataProvider(): array
    {
        return [
            'When first name is too short' => [
                'firstName' => 's',
                'lastName' => 'Smith',
            ],
            'When last name is too short' => [
                'firstName' => 'Andrew',
                'lastName' => 's',
            ],
            'When first name is too long' => [
                'firstName' => '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901',
                'lastName' => 'Smith',
            ],
            'When last name is too long' => [
                'firstName' => 'Andrew',
                'lastName' => '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901',
            ],
        ];
    }
}
