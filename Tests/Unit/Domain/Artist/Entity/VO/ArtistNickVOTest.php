<?php
declare(strict_types=1);

namespace App\Tests\Unit\Domain\Artist\Entity\VO;

use App\Domain\Artist\Entity\VO\ArtistNickVO;
use App\Domain\Artist\Exception\ArtistInvalidArgumentException;
use PHPUnit\Framework\TestCase;

final class ArtistNickVOTest extends TestCase
{
    /**
     * @test
     */
    public function testWhenTryToCreateWithValidDataExpectsCreated(): void
    {
        $artistNameVO = ArtistNickVO::create('test');
        $this->assertEquals('test', $artistNameVO->getNick());
    }

    /**
     * @test
     * @dataProvider invalidDataProvider
     */
    public function testWhenTryToCreateWithInvalidDataExpectsArtistInvalidArgumentException(
        string $nick
    ): void {
        $this->expectException(ArtistInvalidArgumentException::class);
        ArtistNickVO::create($nick);
    }

    public function invalidDataProvider(): array
    {
        return [
            'When nick is too short' => [
                'nick' => '',
            ],
            'When nick is too long' => [
                'nick' => '123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901',
            ],
        ];
    }
}
