# Music crud
Application to manage songs, artists and providers

## Requirements
- Docker
- Docker-compose

## Installation
- Run `make init`
- Run `make start`

## Technologies
- Postgres
- PHP

## Headers
- **token** - To use API we need to in system we use Oauth2 authentication 
- **lang** - (en|pl) Application return responses in selected language

## Tests
### Unit
- make unit

### Behat
- Change `.env.test.dist` to `.env.test`
- Change url to connect with database (e.g. use config from .env and add suffix to database name)
- Run `make behat`
