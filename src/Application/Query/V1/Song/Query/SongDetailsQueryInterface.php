<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Song\Query;

use App\Application\Query\V1\Song\Model\SongDetailsModel;

interface SongDetailsQueryInterface
{
    public function __invoke(string $id, bool $active = true): ?SongDetailsModel;
}
