<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Song\Query;

use Doctrine\Common\Collections\ArrayCollection;

interface SongListQueryInterface
{
    public function __invoke(bool $active = true): ArrayCollection;
}
