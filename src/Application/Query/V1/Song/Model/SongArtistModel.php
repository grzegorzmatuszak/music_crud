<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Song\Model;

use Hateoas\Configuration\Annotation as Hateoas;

/**
 * @Hateoas\Relation(
 *     "api_get_artist",
 *     href = @Hateoas\Route(
 *         "api_get_artist",
 *         parameters = { "id" = "expr(object.getId())" }
 *     )
 * )
 */
final class SongArtistModel
{
    private string $id;

    private string $firstName;

    private string $lastNme;

    private string $nick;

    public function __construct(
        string $id,
        string $firstName,
        string $lastNme,
        string $nick
    ) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastNme = $lastNme;
        $this->nick = $nick;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastNme(): string
    {
        return $this->lastNme;
    }

    public function getNick(): string
    {
        return $this->nick;
    }
}
