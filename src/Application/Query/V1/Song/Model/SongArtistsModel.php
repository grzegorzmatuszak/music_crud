<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Song\Model;

use Doctrine\Common\Collections\Collection;
use Hateoas\Configuration\Annotation as Hateoas;

/**
 * @Hateoas\Relation(
 *      "self",
 *      href=@Hateoas\Route(
 *          "api_get_song_artists",
 *          parameters = { "id"= "expr(object.getId())" },
 *      ),
 * )
 */
final class SongArtistsModel
{
    private string $id;

    private Collection $artists;

    public function __construct(string $id, Collection $artists)
    {
        $this->id = $id;
        $this->artists = $artists;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getArtists(): Collection
    {
        return $this->artists;
    }
}
