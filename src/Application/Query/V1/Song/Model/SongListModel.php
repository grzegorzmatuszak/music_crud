<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Song\Model;

use Hateoas\Configuration\Annotation as Hateoas;

/**
 * @Hateoas\Relation(
 *      "self",
 *      href=@Hateoas\Route(
 *          "api_get_song",
 *          parameters = { "id"= "expr(object.getId())" },
 *      ),
 * ),
 * @Hateoas\Relation(
 *     "artists",
 *     href = @Hateoas\Route(
 *         "api_get_song_artists",
 *         parameters = { "id" = "expr(object.getId())" }
 *     )
 * )
 */
final class SongListModel
{
    private string $id;

    private string $title;

    private string $genre;

    private string $ismn;

    private int $year;

    public function __construct(string $id, string $title, string $genre, string $ismn, int $year)
    {
        $this->id = $id;
        $this->title = $title;
        $this->genre = $genre;
        $this->ismn = $ismn;
        $this->year = $year;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getGenre(): string
    {
        return $this->genre;
    }

    public function getIsmn(): string
    {
        return $this->ismn;
    }

    public function getYear(): int
    {
        return $this->year;
    }
}
