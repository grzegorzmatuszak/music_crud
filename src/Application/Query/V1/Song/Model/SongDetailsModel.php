<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Song\Model;

use App\Application\Query\V1\Publisher\Model\PublisherDetailsModel;
use Doctrine\Common\Collections\ArrayCollection;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Hateoas\Relation(
 *      "self",
 *      href=@Hateoas\Route(
 *          "api_get_song",
 *          parameters = { "id"= "expr(object.getId())" },
 *      ),
 * )
 * @Hateoas\Relation(
 *     "song_artists",
 *     href = @Hateoas\Route(
 *         "api_get_song_artists",
 *         parameters = { "id" = "expr(object.getId())" }
 *     ),
 *     embedded = @Hateoas\Embedded(
 *         "expr(object.getArtists())",
 *          exclusion = @Hateoas\Exclusion(
 *              excludeIf = "expr(object.getArtists().isEmpty() === true)",
 *              maxDepth = 1
 *          )
 *     ),
 * )
 * @Hateoas\Relation(
 *      "publisher",
 *      href=@Hateoas\Route(
 *          "api_get_publisher",
 *          parameters = { "id"= "expr(object.getPublisher().getId())" },
 *      ),
 *     exclusion = @Hateoas\Exclusion(
 *           excludeIf = "expr(object.getPublisher() === null)"
 *      ),
 *      embedded = "expr(object.getPublisher())"
 * )
 */
final class SongDetailsModel
{
    private string $title;

    private string $id;

    private string $genre;

    private string $ismn;

    private int $year;

    /**
     * @Serializer\Exclude()
     */
    private PublisherDetailsModel $publisher;

    /**
     * @Serializer\Exclude()
     */
    private ArrayCollection $artists;

    public function __construct(
        string $id,
        string $title,
        string $genre,
        string $ismn,
        int $year,
        PublisherDetailsModel $publisher,
        ?ArrayCollection $artists = null
    ) {
        $artists ??= new ArrayCollection();
        $this->id = $id;
        $this->title = $title;
        $this->genre = $genre;
        $this->ismn = $ismn;
        $this->year = $year;
        $this->publisher = $publisher;
        $this->artists = $artists;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getGenre(): string
    {
        return $this->genre;
    }

    public function getIsmn(): string
    {
        return $this->ismn;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getPublisher(): PublisherDetailsModel
    {
        return $this->publisher;
    }

    public function getArtists(): ArrayCollection
    {
        return $this->artists;
    }
}
