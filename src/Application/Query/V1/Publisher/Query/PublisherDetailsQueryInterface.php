<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Publisher\Query;

use App\Application\Query\V1\Publisher\Model\PublisherDetailsModel;

interface PublisherDetailsQueryInterface
{
    public function __invoke(string $id, bool $active = true): ?PublisherDetailsModel;
}
