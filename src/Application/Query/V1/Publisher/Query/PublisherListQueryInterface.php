<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Publisher\Query;

use Doctrine\Common\Collections\ArrayCollection;

interface PublisherListQueryInterface
{
    public function __invoke(bool $active = true): ArrayCollection;
}
