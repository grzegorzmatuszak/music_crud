<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Publisher\Model;

use Hateoas\Configuration\Annotation as Hateoas;

/**
 * @Hateoas\Relation(
 *      "self",
 *      href=@Hateoas\Route(
 *          "api_get_publisher",
 *          parameters = { "id"= "expr(object.getId())" },
 *      ),
 * ),
 * @Hateoas\Relation(
 *     "songs",
 *     href = @Hateoas\Route(
 *         "api_get_publisher_songs",
 *         parameters = { "id" = "expr(object.getId())" }
 *     )
 * )
 */
final class PublisherListModel
{
    private string $id;

    private string $name;

    private string $nip;

    private string $address;

    public function __construct(string $id, string $name, string $nip, string $address)
    {
        $this->id = $id;
        $this->name = $name;
        $this->nip = $nip;
        $this->address = $address;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNip(): string
    {
        return $this->nip;
    }

    public function getAddress(): string
    {
        return $this->address;
    }
}
