<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Publisher\Model;

use Doctrine\Common\Collections\Collection;
use Hateoas\Configuration\Annotation as Hateoas;

/**
 * @Hateoas\Relation(
 *      "self",
 *      href=@Hateoas\Route(
 *          "api_get_publisher_songs",
 *          parameters = { "id"= "expr(object.getId())" },
 *      ),
 * )
 */
final class PublisherSongsModel
{
    private string $id;

    private Collection $songs;

    public function __construct(string $id, Collection $songs)
    {
        $this->id = $id;
        $this->songs = $songs;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getSongs(): Collection
    {
        return $this->songs;
    }
}
