<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Publisher\Model;

use Hateoas\Configuration\Annotation as Hateoas;

/**
 * @Hateoas\Relation(
 *     "api_get_song",
 *     href = @Hateoas\Route(
 *         "api_get_song",
 *         parameters = { "id" = "expr(object.getId())" }
 *     )
 * )
 */
final class PublisherSongModel
{
    private string $id;

    private string $title;

    private string $genre;

    private string $ismn;

    private int $year;

    public function __construct(
        string $id,
        string $title,
        string $genre,
        string $ismn,
        int $year
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->genre = $genre;
        $this->ismn = $ismn;
        $this->year = $year;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getGenre(): string
    {
        return $this->genre;
    }

    public function getIsmn(): string
    {
        return $this->ismn;
    }

    public function getYear(): int
    {
        return $this->year;
    }
}
