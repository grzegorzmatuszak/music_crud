<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Artist\Model;

use Hateoas\Configuration\Annotation as Hateoas;

/**
 * @Hateoas\Relation(
 *      "self",
 *      href=@Hateoas\Route(
 *          "api_get_artist",
 *          parameters = { "id"= "expr(object.getId())" },
 *      ),
 * ),
 * @Hateoas\Relation(
 *     "songs",
 *     href = @Hateoas\Route(
 *         "api_get_artist_songs",
 *         parameters = { "id" = "expr(object.getId())" }
 *     )
 * )
 */
final class ArtistListModel
{
    private string $id;

    private string $firstName;

    private string $lastName;

    private string $nick;

    public function __construct(string $id, string $firstName, string $lastName, string $nick)
    {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->nick = $nick;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getNick(): string
    {
        return $this->nick;
    }
}
