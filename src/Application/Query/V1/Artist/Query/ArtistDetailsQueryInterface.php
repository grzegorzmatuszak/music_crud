<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Artist\Query;

use App\Application\Query\V1\Artist\Model\ArtistDetailsModel;

interface ArtistDetailsQueryInterface
{
    public function __invoke(string $id, bool $active = true): ?ArtistDetailsModel;
}
