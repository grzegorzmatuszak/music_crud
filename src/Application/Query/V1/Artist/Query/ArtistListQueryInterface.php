<?php
declare(strict_types=1);

namespace App\Application\Query\V1\Artist\Query;

use Doctrine\Common\Collections\ArrayCollection;

interface ArtistListQueryInterface
{
    public function __invoke(bool $active = true): ArrayCollection;
}
