<?php
declare(strict_types=1);

namespace App\Application\VO;

abstract class UuidVO
{
    private ?string  $id;

    protected function __construct(?string $id)
    {
        $this->id = $id;
    }

    abstract public static function create(?string $id): self;

    public function __toString(): ?string
    {
        return $this->id ?? '';
    }

    public function getId(): ?string
    {
        return $this->id;
    }
}
