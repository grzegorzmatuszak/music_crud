<?php
declare(strict_types=1);

namespace App\Application\Service\Generator\UniqId;

/**
 * Basic way to deliver unique ids for database
 */
interface UuidV4GeneratorInterface
{
    public function generate(): string;
}
