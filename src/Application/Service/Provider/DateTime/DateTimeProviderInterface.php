<?php
declare(strict_types=1);

namespace App\Application\Service\Provider\DateTime;

use DateTime;

interface DateTimeProviderInterface
{
    public function get(): DateTime;
}
