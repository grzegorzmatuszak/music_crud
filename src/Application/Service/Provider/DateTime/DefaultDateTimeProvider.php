<?php
declare(strict_types=1);

namespace App\Application\Service\Provider\DateTime;

use DateTime;

class DefaultDateTimeProvider implements DateTimeProviderInterface
{
    private ?DateTime $date = null;

    public function get(): DateTime
    {
        if (!$this->date) {
            $this->date = new DateTime();
        }

        return clone $this->date;
    }
}
