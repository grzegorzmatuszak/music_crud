<?php
declare(strict_types=1);

namespace App\Application\Command\Song\Update;

use App\Domain\Song\Repository\UpdateSongRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UpdateSongCommandHandler implements MessageHandlerInterface
{
    private UpdateSongRepositoryInterface $updateSongRepository;

    public function __construct(UpdateSongRepositoryInterface $updateSongRepository)
    {
        $this->updateSongRepository = $updateSongRepository;
    }

    public function __invoke(UpdateSongCommand $message): void
    {
        $this->updateSongRepository->update(
            $message->getId(),
            $message->getTitle(),
            $message->getGenre(),
            $message->getIsmn(),
            $message->getYear(),
            $message->getPublisherId(),
            $message->getArtistsIds()
        );
    }
}
