<?php
declare(strict_types=1);

namespace App\Application\Command\Song\Delete;

use App\Domain\Song\Repository\UpdateSongRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteSongCommandHandler implements MessageHandlerInterface
{
    private UpdateSongRepositoryInterface $updateSongRepository;

    public function __construct(UpdateSongRepositoryInterface $updateSongRepository)
    {
        $this->updateSongRepository = $updateSongRepository;
    }

    public function __invoke(DeleteSongCommand $message): void
    {
        $this->updateSongRepository->deleteSong($message->getId());
    }
}
