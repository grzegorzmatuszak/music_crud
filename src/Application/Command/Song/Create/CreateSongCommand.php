<?php
declare(strict_types=1);

namespace App\Application\Command\Song\Create;

final class CreateSongCommand
{
    private string $id;

    private string $title;

    private string $genre;

    private string $ismn;

    private int $year;

    private string $publisherId;

    private array $artistsIds;

    public function __construct(
        string $id,
        string $title,
        string $genre,
        string $ismn,
        int $year,
        string $publisherId,
        array $artistsIds = []
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->genre = $genre;
        $this->ismn = $ismn;
        $this->year = $year;
        $this->publisherId = $publisherId;
        $this->artistsIds = $artistsIds;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getGenre(): string
    {
        return $this->genre;
    }

    public function getIsmn(): string
    {
        return $this->ismn;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getPublisherId(): string
    {
        return $this->publisherId;
    }

    public function getArtistsIds(): array
    {
        return $this->artistsIds;
    }
}
