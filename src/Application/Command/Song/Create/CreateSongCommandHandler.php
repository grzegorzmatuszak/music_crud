<?php
declare(strict_types=1);

namespace App\Application\Command\Song\Create;

use App\Domain\Song\Repository\CreateSongRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateSongCommandHandler implements MessageHandlerInterface
{
    private CreateSongRepositoryInterface $createSongRepository;

    public function __construct(CreateSongRepositoryInterface $createSongRepository)
    {
        $this->createSongRepository = $createSongRepository;
    }

    public function __invoke(CreateSongCommand $message): void
    {
        $this->createSongRepository->save(
            $message->getId(),
            $message->getTitle(),
            $message->getGenre(),
            $message->getIsmn(),
            $message->getYear(),
            $message->getPublisherId(),
            $message->getArtistsIds()
        );
    }
}
