<?php
declare(strict_types=1);

namespace App\Application\Command\Publisher\Create;

use App\Domain\Publisher\Repository\CreatePublisherRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreatePublisherCommandHandler implements MessageHandlerInterface
{
    private CreatePublisherRepositoryInterface $createPublisherRepository;

    public function __construct(CreatePublisherRepositoryInterface $createPublisherRepository)
    {
        $this->createPublisherRepository = $createPublisherRepository;
    }

    public function __invoke(CreatePublisherCommand $message): void
    {
        $this->createPublisherRepository->save(
            $message->getId(),
            $message->getName(),
            $message->getNip(),
            $message->getAddress(),
            $message->getSongIds()
        );
    }
}
