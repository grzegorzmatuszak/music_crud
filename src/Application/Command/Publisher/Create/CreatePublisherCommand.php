<?php
declare(strict_types=1);

namespace App\Application\Command\Publisher\Create;

final class CreatePublisherCommand
{
    private string $id;

    private array  $songsIds = [];

    private string $name;

    private string $nip;

    private string $address;

    public function __construct(string $id, string $name, string $nip, string $address, array $songsIds)
    {
        $this->id = $id;
        $this->songsIds = $songsIds;
        $this->name = $name;
        $this->nip = $nip;
        $this->address = $address;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getSongIds(): array
    {
        return $this->songsIds;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNip(): string
    {
        return $this->nip;
    }

    public function getAddress(): string
    {
        return $this->address;
    }
}
