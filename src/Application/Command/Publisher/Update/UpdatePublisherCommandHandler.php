<?php
declare(strict_types=1);

namespace App\Application\Command\Publisher\Update;

use App\Domain\Publisher\Repository\UpdatePublisherRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UpdatePublisherCommandHandler implements MessageHandlerInterface
{
    private UpdatePublisherRepositoryInterface $updatePublisherRepository;

    public function __construct(UpdatePublisherRepositoryInterface $updatePublisherRepository)
    {
        $this->updatePublisherRepository = $updatePublisherRepository;
    }

    public function __invoke(UpdatePublisherCommand $message): void
    {
        $this->updatePublisherRepository->update($message->getId(), $message->getName(), $message->getNip(), $message->getAddress(), $message->getSongIds());
    }
}
