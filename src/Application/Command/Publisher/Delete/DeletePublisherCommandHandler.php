<?php
declare(strict_types=1);

namespace App\Application\Command\Publisher\Delete;

use App\Domain\Publisher\Repository\UpdatePublisherRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeletePublisherCommandHandler implements MessageHandlerInterface
{
    private UpdatePublisherRepositoryInterface $updatePublisherRepository;

    public function __construct(UpdatePublisherRepositoryInterface $updatePublisherRepository)
    {
        $this->updatePublisherRepository = $updatePublisherRepository;
    }

    public function __invoke(DeletePublisherCommand $message): void
    {
        $this->updatePublisherRepository->deletePublisher($message->getId());
    }
}
