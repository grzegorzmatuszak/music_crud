<?php
declare(strict_types=1);

namespace App\Application\Command\Artist\Delete;

use App\Domain\Artist\Repository\UpdateArtistRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteArtistCommandHandler implements MessageHandlerInterface
{
    private UpdateArtistRepositoryInterface $updateArtistRepository;

    public function __construct(UpdateArtistRepositoryInterface $updateArtistRepository)
    {
        $this->updateArtistRepository = $updateArtistRepository;
    }

    public function __invoke(DeleteArtistCommand $message): void
    {
        $this->updateArtistRepository->deleteArtist($message->getId());
    }
}
