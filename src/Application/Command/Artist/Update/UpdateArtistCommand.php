<?php
declare(strict_types=1);

namespace App\Application\Command\Artist\Update;

final class UpdateArtistCommand
{
    private string $id;

    private string $firstName;

    private string $lastName;

    private string $nick;

    private array $songIds;

    public function __construct(string $id, string $firstName, string $lastName, string $nick, array $songIds = [])
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->nick = $nick;
        $this->id = $id;
        $this->songIds = $songIds;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getNick(): string
    {
        return $this->nick;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getSongIds(): array
    {
        return $this->songIds;
    }
}
