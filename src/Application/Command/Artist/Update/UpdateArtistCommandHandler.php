<?php
declare(strict_types=1);

namespace App\Application\Command\Artist\Update;

use App\Domain\Artist\Repository\UpdateArtistRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class UpdateArtistCommandHandler implements MessageHandlerInterface
{
    private UpdateArtistRepositoryInterface $updateArtistRepository;

    public function __construct(UpdateArtistRepositoryInterface $updateArtistRepository)
    {
        $this->updateArtistRepository = $updateArtistRepository;
    }

    public function __invoke(UpdateArtistCommand $message): void
    {
        $this->updateArtistRepository->update($message->getId(), $message->getFirstName(), $message->getLastName(), $message->getNick(), $message->getSongIds());
    }
}
