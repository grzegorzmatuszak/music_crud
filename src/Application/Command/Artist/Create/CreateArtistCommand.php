<?php
declare(strict_types=1);

namespace App\Application\Command\Artist\Create;

final class CreateArtistCommand
{
    private string $id;

    private string $firstName;

    private string $lastName;

    private string $nick;

    private array  $songsIds = [];

    public function __construct(string $id, string $firstName, string $lastName, string $nick, array $songsIds)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->nick = $nick;
        $this->id = $id;
        $this->songsIds = $songsIds;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getNick(): string
    {
        return $this->nick;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getSongIds(): array
    {
        return $this->songsIds;
    }
}
