<?php
declare(strict_types=1);

namespace App\Application\Command\Artist\Create;

use App\Domain\Artist\Repository\CreateArtistRepositoryInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateArtistCommandHandler implements MessageHandlerInterface
{
    private CreateArtistRepositoryInterface $createArtistRepository;

    public function __construct(CreateArtistRepositoryInterface $createArtistRepository)
    {
        $this->createArtistRepository = $createArtistRepository;
    }

    public function __invoke(CreateArtistCommand $message): void
    {
        $this->createArtistRepository->save(
            $message->getId(),
            $message->getFirstName(),
            $message->getLastName(),
            $message->getNick(),
            $message->getSongIds()
        );
    }
}
