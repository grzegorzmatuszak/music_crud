<?php
declare(strict_types=1);

namespace App\Infrastructure\Song\Query;

use App\Application\Query\V1\Artist\Model\ArtistDetailsModel;
use App\Application\Query\V1\Publisher\Model\PublisherDetailsModel;
use App\Application\Query\V1\Song\Model\SongDetailsModel;
use App\Application\Query\V1\Song\Query\SongDetailsQueryInterface;
use App\Domain\Artist\Entity\VO\ArtistIdVO;
use App\Domain\Song\Entity\Song;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

final class SongDetailsQuery implements SongDetailsQueryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(string $id, bool $active = true): ?SongDetailsModel
    {
        $query = $this->entityManager->createQueryBuilder();
        $query
            ->select('s.id, s.title.title as title, s.genre.genre as genre, s.ismn.ismn as ismn, s.date.year as year, p.id as pid, p.name.name as pname, p.nip.nip as pnip, p.address.address as paddress, a.id as artistId, a.name.firstName as aFirstName, a.name.lastName as aLastName, a.nick.nick as aNick')
            ->from(Song::class, 's')
            ->leftJoin('s.publisher', 'p', 'WITH')
            ->leftJoin('s.artists', 'a', 'WITH', 'a.active = :active')
            ->andWhere('s.id = :id')
            ->setParameter('id', $id);

        if ($active) {
            $query
                ->andWhere('s.active = :active')
                ->setParameter('active', true);
        }
        $resultList = $query->getQuery()->getResult();
        if (empty($resultList)) {
            return null;
        }
        $result = reset($resultList);

        $artistCollection = new ArrayCollection();
        foreach ($resultList as $result) {
            if ($result['artistId'] instanceof ArtistIdVO && !is_null($result['artistId']->getId())) {
                $artistCollection->add(new ArtistDetailsModel($result['artistId']->getId(), $result['aFirstName'], $result['aLastName'], $result['aNick']));
            }
        }

        return new SongDetailsModel(
            $result['id']->getId(),
            $result['title'], $result['genre'],
            $result['ismn'],
            (int) $result['year'],
            new PublisherDetailsModel($result['pid']->getId(), $result['pname'], $result['pnip'], $result['paddress']),
            $artistCollection
        );
    }
}
