<?php
declare(strict_types=1);

namespace App\Infrastructure\Song\Query;

use App\Application\Query\V1\Song\Model\SongArtistModel;
use App\Application\Query\V1\Song\Model\SongArtistsModel;
use App\Application\Query\V1\Song\Query\SongArtistsQueryInterface;
use App\Domain\Artist\Entity\VO\ArtistIdVO;
use App\Domain\Song\Entity\Song;
use App\Domain\Song\Entity\VO\SongIdVO;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class SongArtistsQuery implements SongArtistsQueryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(string $id, bool $active = true): ?SongArtistsModel
    {
        $query = $this->entityManager->createQueryBuilder();

        $query
            ->select('s.id as songId, a.id as artistId, a.name.firstName as firstName, a.name.lastName as lastName, a.nick.nick as nick')
            ->from(Song::class, 's')
            ->leftJoin('s.artists', 'a', 'WITH', 'a.active = :active');
        if ($active) {
            $query->andWhere('s.active = :active');
            $query->setParameter('active', true);
        }
        $query->andWhere('s.id = :id');
        $query->setParameter('id', $id);

        $resultList = $query->getQuery()->getResult();

        if (empty($resultList)) {
            return null;
        }

        $songCollection = new ArrayCollection();

        foreach ($resultList as $result) {
            if ($result['artistId'] instanceof ArtistIdVO && !is_null($result['artistId']->getId())) {
                $songCollection->add(new SongArtistModel($result['artistId']->getId(), $result['firstName'], $result['lastName'], $result['nick']));
            }
        }

        return new SongArtistsModel(reset($resultList)['songId']->getId(), $songCollection);
    }
}
