<?php
declare(strict_types=1);

namespace App\Infrastructure\Song\Query;

use App\Application\Query\V1\Song\Model\SongListModel;
use App\Application\Query\V1\Song\Query\SongListQueryInterface;
use App\Domain\Song\Entity\Song;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

final class SongListQuery implements SongListQueryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(bool $active = true): ArrayCollection
    {
        $query = $this->entityManager->createQueryBuilder();
        $query
            ->select('s.id, s.title.title as title, s.genre.genre as genre, s.ismn.ismn as ismn, s.date.year as year')
            ->from(Song::class, 's');

        if ($active) {
            $query
                ->andWhere('s.active = :active')
                ->setParameter('active', true);
        }
        $resultList = $query->getQuery()->getResult();

        $collectionResult = new ArrayCollection();
        foreach ($resultList as $result) {
            $collectionResult->add(new SongListModel($result['id']->getId(), $result['title'], $result['genre'], $result['ismn'], (int) $result['year']));
        }

        return $collectionResult;
    }
}
