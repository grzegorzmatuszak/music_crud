<?php
declare(strict_types=1);

namespace App\Infrastructure\Song\ORM\Type;

use App\Domain\Song\Entity\VO\SongIdVO;
use App\Infrastructure\Shared\ORM\Type\AbstractIdType;

class SongIdType extends AbstractIdType
{
    const SONG_ID = 'song_id';

    public function getName()
    {
        return static::SONG_ID;
    }

    protected function getValueObjectClassName(): string
    {
        return SongIdVO::class;
    }
}
