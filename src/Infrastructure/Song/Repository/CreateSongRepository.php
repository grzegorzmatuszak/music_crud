<?php
declare(strict_types=1);

namespace App\Infrastructure\Song\Repository;

use App\Domain\Artist\Repository\GetArtistRepositoryInterface;
use App\Domain\Publisher\Repository\GetPublisherRepositoryInterface;
use App\Domain\Song\Entity\Song;
use App\Domain\Song\Entity\VO\SongDateVO;
use App\Domain\Song\Entity\VO\SongGenreVO;
use App\Domain\Song\Entity\VO\SongIdVO;
use App\Domain\Song\Entity\VO\SongIsmnVO;
use App\Domain\Song\Entity\VO\SongTitleVO;
use App\Domain\Song\Repository\CreateSongRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

final class CreateSongRepository implements CreateSongRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    private GetArtistRepositoryInterface $getArtistRepository;

    private GetPublisherRepositoryInterface $getPublisherRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        GetArtistRepositoryInterface $getArtistRepository,
        GetPublisherRepositoryInterface $getPublisherRepository
    ) {
        $this->entityManager = $entityManager;
        $this->getArtistRepository = $getArtistRepository;
        $this->getPublisherRepository = $getPublisherRepository;
    }

    public function save(
        string $id,
        string $title,
        string $genre,
        string $ismn,
        int $year,
        string $publisherId,
        array $artistsIds = []
    ): void {
        $publisher = $this->getPublisherRepository->findById($publisherId);
        $song = Song::create(SongIdVO::create($id), SongTitleVO::create($title), SongGenreVO::create($genre), SongIsmnVO::create($ismn), SongDateVO::create($year), $publisher);

        if (!empty($artistsIds)) {
            $resultList = $this->getArtistRepository->findByIds($artistsIds);
            foreach ($resultList as $result) {
                $result->addSong($song);
            }
        }

        $this->entityManager->persist($song);
    }
}
