<?php
declare(strict_types=1);

namespace App\Infrastructure\Song\Repository;

use App\Domain\Shared\Exception\NotFoundException;
use App\Domain\Song\Entity\Song;
use App\Domain\Song\Repository\GetSongRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class GetSongRepository extends ServiceEntityRepository implements GetSongRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Song::class);
    }

    public function findById(string $id, bool $active = true): Song
    {
        $qb = $this->createQueryBuilder('s')
            ->andWhere('s.id = :id')
            ->setParameter('id', $id);

        if ($active) {
            $qb->andWhere('s.active = :active');
            $qb->setParameter('active', true);
        }

        $result = $qb
            ->getQuery()
            ->getResult();

        if (empty($result)) {
            throw new NotFoundException('Song not found');
        }

        return reset($result);
    }

    public function findByIds(array $idList, bool $active = true): array
    {
        $qb = $this->createQueryBuilder('s')
            ->andWhere('s.id IN (:idList)')
            ->setParameter('idList', $idList);

        if ($active) {
            $qb->andWhere('s.active = :active');
            $qb->setParameter('active', true);
        }

        return $qb
            ->getQuery()
            ->getResult();
    }
}
