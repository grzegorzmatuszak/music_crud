<?php
declare(strict_types=1);

namespace App\Infrastructure\Song\Repository;

use App\Domain\Artist\Entity\Artist;
use App\Domain\Artist\Repository\GetArtistRepositoryInterface;
use App\Domain\Publisher\Repository\GetPublisherRepositoryInterface;
use App\Domain\Song\Entity\Song;
use App\Domain\Song\Entity\VO\SongDateVO;
use App\Domain\Song\Entity\VO\SongGenreVO;
use App\Domain\Song\Entity\VO\SongIsmnVO;
use App\Domain\Song\Entity\VO\SongTitleVO;
use App\Domain\Song\Repository\GetSongRepositoryInterface;
use App\Domain\Song\Repository\UpdateSongRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

final class UpdateSongRepository implements UpdateSongRepositoryInterface
{
    private GetArtistRepositoryInterface $getArtistRepository;

    private GetSongRepositoryInterface $getSongRepository;

    private EntityManagerInterface $entityManager;

    private GetPublisherRepositoryInterface $getPublisherRepository;

    public function __construct(
        GetArtistRepositoryInterface $getArtistRepository,
        GetSongRepositoryInterface $getSongRepository,
        GetPublisherRepositoryInterface $getPublisherRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->getArtistRepository = $getArtistRepository;
        $this->getSongRepository = $getSongRepository;
        $this->entityManager = $entityManager;
        $this->getPublisherRepository = $getPublisherRepository;
    }

    public function deleteSong(string $id): void
    {
        $artist = $this->getSongRepository->findById($id);
        $artist->deactivate();
    }

    public function update(
        string $id,
        string $title,
        string $genre,
        string $ismn,
        int $year,
        string $publisherId,
        array $artistsIds = []
    ): void {
        /** @var Song $song */
        $song = $this->getSongRepository->findById($id);
        $publisher = $this->getPublisherRepository->findById($publisherId);

        $song->setTitle(SongTitleVO::create($title));
        $song->setGenre(SongGenreVO::create($genre));
        $song->setIsmn(SongIsmnVO::create($ismn));
        $song->setDate(SongDateVO::create($year));
        $song->setPublisher($publisher);
        /** @var Artist $artist */
        foreach ($song->getArtists() as $artist) {
            $artist->removeSong($song);
        }


        if (!empty($artistsIds)) {
            $resultList = $this->getArtistRepository->findByIds($artistsIds);
            foreach ($resultList as $result) {
                $result->addSong($song);
            }
        }

        $this->entityManager->persist($song);
    }
}
