<?php
declare(strict_types=1);

namespace App\Infrastructure\DataFixtures;

use App\Domain\Publisher\Entity\Publisher;
use App\Domain\Publisher\Entity\VO\PublisherAddressVO;
use App\Domain\Publisher\Entity\VO\PublisherIdVO;
use App\Domain\Publisher\Entity\VO\PublisherNameVO;
use App\Domain\Publisher\Entity\VO\PublisherNipVO;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

final class PublisherFixtures extends Fixture
{
    const PUBLISHER_1 = 'publisher_1';
    const PUBLISHER_2 = 'publisher_2';
    const PUBLISHER_3 = 'publisher_3';
    const PUBLISHER_4 = 'publisher_4';
    const PUBLISHER_5 = 'publisher_5';

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function load(ObjectManager $manager)
    {
        $id = '11423848-458d-4d4b-beeb-7317b703d212';
        $publisher = Publisher::create(PublisherIdVO::create($id), PublisherNameVO::create('sone publiuszer'), PublisherNipVO::create('nip'), PublisherAddressVO::create('address '));
        $this->addReference(self::PUBLISHER_1, $publisher);
        $this->entityManager->persist($publisher);

        $id = '0660083a-3546-4143-ae74-c73bc06209e1';
        $publisher = Publisher::create(PublisherIdVO::create($id), PublisherNameVO::create('sone publi'), PublisherNipVO::create('nip2'), PublisherAddressVO::create('addressess '));
        $this->addReference(self::PUBLISHER_2, $publisher);
        $this->entityManager->persist($publisher);

        $id = '0660083a-3546-4143-ae74-c73bc06209e2';
        $publisher = Publisher::create(PublisherIdVO::create($id), PublisherNameVO::create('sone publi'), PublisherNipVO::create('nip2'), PublisherAddressVO::create('addressess '));
        $this->addReference(self::PUBLISHER_3, $publisher);
        $this->entityManager->persist($publisher);


        $id = '1660083a-3546-4143-ae74-c73bc06209e2';
        $publisher = Publisher::create(PublisherIdVO::create($id), PublisherNameVO::create('empty song publi'), PublisherNipVO::create('nip2'), PublisherAddressVO::create('addressess '));
        $this->addReference(self::PUBLISHER_4, $publisher);
        $this->entityManager->persist($publisher);

        $id = '2660083a-3546-4143-ae74-c73bc06209e2';
        $publisher = Publisher::create(PublisherIdVO::create($id), PublisherNameVO::create(' publi'), PublisherNipVO::create('nip2'), PublisherAddressVO::create('addressess '));
        $this->addReference(self::PUBLISHER_5, $publisher);
        $this->entityManager->persist($publisher);

        $id = '3660083a-3546-4143-ae74-c73bc06209e2';
        $publisher = Publisher::create(PublisherIdVO::create($id), PublisherNameVO::create(' remove it'), PublisherNipVO::create('nip2'), PublisherAddressVO::create('addressess '));
        $this->entityManager->persist($publisher);

        $this->entityManager->flush();
    }
}
