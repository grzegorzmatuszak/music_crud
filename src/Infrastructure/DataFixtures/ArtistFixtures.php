<?php
declare(strict_types=1);

namespace App\Infrastructure\DataFixtures;

use App\Domain\Artist\Entity\Artist;
use App\Domain\Artist\Entity\VO\ArtistIdVO;
use App\Domain\Artist\Entity\VO\ArtistNameVO;
use App\Domain\Artist\Entity\VO\ArtistNickVO;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

final class ArtistFixtures extends Fixture implements DependentFixtureInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function load(ObjectManager $manager)
    {
        $id = '0660083a-3546-4143-ae74-c73bc06209e7';
        $artist = Artist::create(ArtistIdVO::create($id), ArtistNameVO::create('firstName', 'lastName'), ArtistNickVO::create('nick'));
        $artist->addSong($this->getReference(SongFixtures::SONG_1));
        $artist->addSong($this->getReference(SongFixtures::SONG_3));
        $artist->addSong($this->getReference(SongFixtures::SONG_4));
        $this->entityManager->persist($artist);

        $id = '0660083a-3546-4143-ae74-c73bc06209e5';
        $artist = Artist::create(ArtistIdVO::create($id), ArtistNameVO::create('firstName', 'lastName'), ArtistNickVO::create('nick'));
        $artist->addSong($this->getReference(SongFixtures::SONG_1));
        $artist->addSong($this->getReference(SongFixtures::SONG_2));
        $this->entityManager->persist($artist);

        $id = '3f9da869-a230-4fc6-8012-40a5f06d694e';
        $artist = Artist::create(ArtistIdVO::create($id), ArtistNameVO::create('second art', 'last sec'), ArtistNickVO::create('next nicxknick'));
        $artist->addSong($this->getReference(SongFixtures::SONG_1));
        $this->entityManager->persist($artist);

        $id = 'c33b0871-3cd6-46e2-8112-2de246b3ea66';
        $artist = Artist::create(ArtistIdVO::create($id), ArtistNameVO::create('deleted art', 'deleted'), ArtistNickVO::create('deleted nicxknick'));
        $artist->deactivate();
        $this->entityManager->persist($artist);

        $id = 'd9741ab8-784e-45bf-af92-0416e97730da';
        $artist = Artist::create(ArtistIdVO::create($id), ArtistNameVO::create('to delete', 'to delete'), ArtistNickVO::create('to delete'));
        $artist->addSong($this->getReference(SongFixtures::SONG_1));
        $artist->addSong($this->getReference(SongFixtures::SONG_2));
        $this->entityManager->persist($artist);

        $id = 'd9741ab8-784e-45bf-af92-0416e97730dasdx';
        $artist = Artist::create(ArtistIdVO::create($id), ArtistNameVO::create('nex t f ', ' next t l'), ArtistNickVO::create(' next  t n'));
        $artist->addSong($this->getReference(SongFixtures::SONG_3));
        $artist->addSong($this->getReference(SongFixtures::SONG_4));
        $artist->addSong($this->getReference(SongFixtures::SONG_5));
        $this->entityManager->persist($artist);

        $this->entityManager->flush();
    }

    public function getDependencies(): array
    {
        return [
            SongFixtures::class,
        ];
    }
}
