<?php
declare(strict_types=1);

namespace App\Infrastructure\DataFixtures;

use App\Domain\Publisher\Entity\Publisher;
use App\Domain\Song\Entity\Song;
use App\Domain\Song\Entity\VO\SongDateVO;
use App\Domain\Song\Entity\VO\SongGenreVO;
use App\Domain\Song\Entity\VO\SongIdVO;
use App\Domain\Song\Entity\VO\SongIsmnVO;
use App\Domain\Song\Entity\VO\SongTitleVO;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;

final class SongFixtures extends Fixture implements DependentFixtureInterface
{
    const SONG_1 = 'song_1';
    const SONG_2 = 'song_2';
    const SONG_3 = 'song_3';
    const SONG_4 = 'song_4';
    const SONG_5 = 'song_5';

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function load(ObjectManager $manager)
    {
        $id = '11423848-458d-4d4b-beeb-7317b703d212';

        /** @var Publisher $publisher */
        $publisher = $this->getReference(PublisherFixtures::PUBLISHER_1);
        $song = Song::create(SongIdVO::create($id), SongTitleVO::create('some title'), SongGenreVO::create('genre'), SongIsmnVO::create('ismn'), SongDateVO::create(1994), $publisher);
        $this->addReference(self::SONG_1, $song);
        $this->entityManager->persist($song);

        $id = '0660083a-3546-4143-ae74-c73bc06209e1';
        $publisher = $this->getReference(PublisherFixtures::PUBLISHER_2);
        $song = Song::create(SongIdVO::create($id), SongTitleVO::create('other title'), SongGenreVO::create('genre2'), SongIsmnVO::create('ismn3'), SongDateVO::create(1994), $publisher);
        $this->addReference(self::SONG_2, $song);
        $this->entityManager->persist($song);

        $id = '0660083a-3546-4143-ae74-c73bc06209e2';
        $publisher = $this->getReference(PublisherFixtures::PUBLISHER_3);

        $song = Song::create(SongIdVO::create($id), SongTitleVO::create('other title1'), SongGenreVO::create('genre2'), SongIsmnVO::create('ismn3'), SongDateVO::create(1994), $publisher);
        $this->addReference(self::SONG_3, $song);
        $this->entityManager->persist($song);

        $id = '0660083a-3546-4143-ae74-c73bc06209e3';
        $song = Song::create(SongIdVO::create($id), SongTitleVO::create('other title2'), SongGenreVO::create('genre2'), SongIsmnVO::create('ismn3'), SongDateVO::create(1994), $publisher);
        $this->addReference(self::SONG_4, $song);
        $this->entityManager->persist($song);

        $id = '0660083a-3546-4143-ae74-c73bc06209e4';
        $song = Song::create(SongIdVO::create($id), SongTitleVO::create('other title3'), SongGenreVO::create('genre2'), SongIsmnVO::create('ismn3'), SongDateVO::create(1994), $publisher);
        $this->addReference(self::SONG_5, $song);
        $this->entityManager->persist($song);

        $publisher = $this->getReference(PublisherFixtures::PUBLISHER_2);
        $id = '0660083a-3546-4143-ae74-13212451';
        $song = Song::create(SongIdVO::create($id), SongTitleVO::create('without artist'), SongGenreVO::create('genre2'), SongIsmnVO::create('ismn3'), SongDateVO::create(1994), $publisher);
        $this->entityManager->persist($song);

        $this->entityManager->flush();
    }

    public function getDependencies(): array
    {
        return [
            PublisherFixtures::class,
        ];
    }
}
