<?php
declare(strict_types=1);

namespace App\Infrastructure\Publisher\Query;

use App\Application\Query\V1\Publisher\Model\PublisherSongModel;
use App\Application\Query\V1\Publisher\Model\PublisherSongsModel;
use App\Application\Query\V1\Publisher\Query\PublisherSongsQueryInterface;
use App\Domain\Publisher\Entity\Publisher;
use App\Domain\Song\Entity\VO\SongIdVO;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class PublisherSongsQuery implements PublisherSongsQueryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(string $id, bool $active = true): ?PublisherSongsModel
    {
        $query = $this->entityManager->createQueryBuilder();

        $query
            ->select('p.id as publisherId, s.id as songId, s.title.title as title, s.genre.genre as genre, s.ismn.ismn as ismn, s.date.year as year')
            ->from(Publisher::class, 'p')
            ->leftJoin('p.songs', 's', 'WITH', 's.active = :active');
        if ($active) {
            $query->andWhere('p.active = :active');
            $query->setParameter('active', true);
        }
        $query->andWhere('p.id = :id');
        $query->setParameter('id', $id);
        $resultList = $query->getQuery()->getResult();

        if (empty($resultList)) {
            return null;
        }

        $songCollection = new ArrayCollection();
        foreach ($resultList as $result) {
            if ($result['songId'] instanceof SongIdVO && !is_null($result['songId']->getId())) {
                $songCollection->add(new PublisherSongModel($result['songId']->getId(), $result['title'], $result['genre'], $result['ismn'], (int) $result['year']));
            }
        }

        return new PublisherSongsModel(reset($resultList)['publisherId']->getId(), $songCollection);
    }
}
