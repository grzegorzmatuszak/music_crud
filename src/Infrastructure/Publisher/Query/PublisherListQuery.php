<?php
declare(strict_types=1);

namespace App\Infrastructure\Publisher\Query;

use App\Application\Query\V1\Publisher\Model\PublisherListModel;
use App\Application\Query\V1\Publisher\Query\PublisherListQueryInterface;
use App\Domain\Publisher\Entity\Publisher;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

final class PublisherListQuery implements PublisherListQueryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(bool $active = true): ArrayCollection
    {
        $query = $this->entityManager->createQueryBuilder();
        $query
            ->select('p.id, p.name.name as name, p.nip.nip as nip, p.address.address as address')
            ->from(Publisher::class, 'p');

        if ($active) {
            $query
                ->andWhere('p.active = :active')
                ->setParameter('active', true);
        }
        $resultList = $query->getQuery()->getResult();

        $collectionResult = new ArrayCollection();
        foreach ($resultList as $result) {
            $collectionResult->add(new PublisherListModel($result['id']->getId(), $result['name'], $result['nip'], $result['address']));
        }

        return $collectionResult;
    }
}
