<?php
declare(strict_types=1);

namespace App\Infrastructure\Publisher\Query;

use App\Application\Query\V1\Publisher\Model\PublisherDetailsModel;
use App\Application\Query\V1\Publisher\Query\PublisherDetailsQueryInterface;
use App\Domain\Publisher\Entity\Publisher;
use Doctrine\ORM\EntityManagerInterface;

final class PublisherDetailsQuery implements PublisherDetailsQueryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $id
     * @param bool   $active
     *
     * @return PublisherDetailsModel|null
     */
    public function __invoke(string $id, bool $active = true): ?PublisherDetailsModel
    {
        $query = $this->entityManager->createQueryBuilder();
        $query
            ->select('p.id, p.name.name as name, p.nip.nip as nip, p.address.address as address')
            ->from(Publisher::class, 'p')
            ->andWhere('p.id = :id')
            ->setParameter('id', $id);

        if ($active) {
            $query
                ->andWhere('p.active = :active')
                ->setParameter('active', true);
        }

        $result = $query->getQuery()->getResult();
        if (empty($result)) {
            return null;
        }
        $result = reset($result);

        return new PublisherDetailsModel($result['id']->getId(), $result['name'], $result['nip'], $result['address']);
    }
}
