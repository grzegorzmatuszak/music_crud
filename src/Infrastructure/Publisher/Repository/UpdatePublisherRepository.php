<?php
declare(strict_types=1);

namespace App\Infrastructure\Publisher\Repository;

use App\Domain\Publisher\Entity\VO\PublisherAddressVO;
use App\Domain\Publisher\Entity\VO\PublisherNameVO;
use App\Domain\Publisher\Entity\VO\PublisherNipVO;
use App\Domain\Publisher\Repository\GetPublisherRepositoryInterface;
use App\Domain\Publisher\Repository\UpdatePublisherRepositoryInterface;
use App\Domain\Song\Repository\GetSongRepositoryInterface;

final class UpdatePublisherRepository implements UpdatePublisherRepositoryInterface
{
    private GetPublisherRepositoryInterface $getPublisherRepository;

    private GetSongRepositoryInterface $getSongRepository;

    public function __construct(
        GetPublisherRepositoryInterface $getPublisherRepository,
        GetSongRepositoryInterface $getSongRepository
    ) {
        $this->getPublisherRepository = $getPublisherRepository;
        $this->getSongRepository = $getSongRepository;
    }

    public function deletePublisher(string $id): void
    {
        $publisher = $this->getPublisherRepository->findById($id);
        $publisher->deactivate();
    }

    public function update(
        string $id,
        string $name,
        string $nip,
        string $address,
        array $songsIds = []
    ): void {
        $publisher = $this->getPublisherRepository->findById($id);
        if (!empty($songsIds)) {
            $resultList = $this->getSongRepository->findByIds($songsIds);
            foreach ($resultList as $result) {
                $publisher->addSong($result);
            }
        }
        $publisher->setName(PublisherNameVO::create($name));
        $publisher->setNip(PublisherNipVO::create($nip));
        $publisher->setAddress(PublisherAddressVO::create($address));
    }
}
