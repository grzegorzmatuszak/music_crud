<?php
declare(strict_types=1);

namespace App\Infrastructure\Publisher\Repository;

use App\Domain\Publisher\Entity\Publisher;
use App\Domain\Publisher\Entity\VO\PublisherAddressVO;
use App\Domain\Publisher\Entity\VO\PublisherIdVO;
use App\Domain\Publisher\Entity\VO\PublisherNameVO;
use App\Domain\Publisher\Entity\VO\PublisherNipVO;
use App\Domain\Publisher\Repository\CreatePublisherRepositoryInterface;
use App\Domain\Song\Repository\GetSongRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

final class CreatePublisherRepository implements CreatePublisherRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    private GetSongRepositoryInterface $getSongRepository;

    public function __construct(EntityManagerInterface $entityManager, GetSongRepositoryInterface $getSongRepository)
    {
        $this->entityManager = $entityManager;
        $this->getSongRepository = $getSongRepository;
    }

    public function save(
        string $id,
        string $name,
        string $nip,
        string $address,
        array $songsIds = []
    ): void {
        $publisher = Publisher::create(PublisherIdVO::create($id), PublisherNameVO::create($name), PublisherNipVO::create($nip), PublisherAddressVO::create($address));

        if (!empty($songsIds)) {
            $resultList = $this->getSongRepository->findByIds($songsIds);
            foreach ($resultList as $result) {
                $publisher->addSong($result);
            }
        }

        $this->entityManager->persist($publisher);
    }
}
