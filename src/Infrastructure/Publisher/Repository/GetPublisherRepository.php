<?php
declare(strict_types=1);

namespace App\Infrastructure\Publisher\Repository;

use App\Domain\Publisher\Entity\Publisher;
use App\Domain\Publisher\Repository\GetPublisherRepositoryInterface;
use App\Domain\Shared\Exception\NotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class GetPublisherRepository extends ServiceEntityRepository implements GetPublisherRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Publisher::class);
    }

    public function findById(string $id, bool $active = true): Publisher
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.id = :id')
            ->setParameter('id', $id);

        if ($active) {
            $qb->andWhere('p.active = :active');
            $qb->setParameter('active', true);
        }

        $result = $qb
            ->getQuery()
            ->getResult();

        if (empty($result)) {
            throw new NotFoundException('Publisher not found');
        }

        return reset($result);
    }

    public function findByIds(array $idList, bool $active = true): array
    {
        $qb = $this->createQueryBuilder('s')
            ->andWhere('p.id IN (:idList)')
            ->setParameter('idList', $idList);

        if ($active) {
            $qb->andWhere('p.active = :active');
            $qb->setParameter('active', true);
        }

        return $qb
            ->getQuery()
            ->getResult();
    }
}
