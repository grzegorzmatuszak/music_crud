<?php
declare(strict_types=1);

namespace App\Infrastructure\Publisher\ORM\Type;

use App\Domain\Publisher\Entity\VO\PublisherIdVO;
use App\Infrastructure\Shared\ORM\Type\AbstractIdType;

class PublisherIdType extends AbstractIdType
{
    const PUBLISHER_ID = 'publisher_id';

    public function getName()
    {
        return static::PUBLISHER_ID;
    }

    protected function getValueObjectClassName(): string
    {
        return PublisherIdVO::class;
    }
}
