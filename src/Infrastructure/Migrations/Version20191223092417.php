<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191223092417 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE artist (id VARCHAR(255) NOT NULL, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL, nick VARCHAR(255) NOT NULL, active BOOLEAN DEFAULT \'true\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE artists_songs (artist_id VARCHAR(255) NOT NULL, song_id VARCHAR(255) NOT NULL, PRIMARY KEY(artist_id, song_id))');
        $this->addSql('CREATE INDEX IDX_CACB0291B7970CF8 ON artists_songs (artist_id)');
        $this->addSql('CREATE INDEX IDX_CACB0291A0BDB2F3 ON artists_songs (song_id)');
        $this->addSql('CREATE TABLE song (id VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, genre VARCHAR(255) NOT NULL, ismn VARCHAR(255) NOT NULL, year VARCHAR(4) NOT NULL, active BOOLEAN DEFAULT \'true\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE artists_songs ADD CONSTRAINT FK_CACB0291B7970CF8 FOREIGN KEY (artist_id) REFERENCES artist (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE artists_songs ADD CONSTRAINT FK_CACB0291A0BDB2F3 FOREIGN KEY (song_id) REFERENCES song (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE artists_songs DROP CONSTRAINT FK_CACB0291B7970CF8');
        $this->addSql('ALTER TABLE artists_songs DROP CONSTRAINT FK_CACB0291A0BDB2F3');
        $this->addSql('DROP TABLE artist');
        $this->addSql('DROP TABLE artists_songs');
        $this->addSql('DROP TABLE song');
    }
}
