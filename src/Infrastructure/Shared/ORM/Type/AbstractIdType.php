<?php
declare(strict_types=1);

namespace App\Infrastructure\Shared\ORM\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\Type;

abstract class AbstractIdType extends Type
{
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        $fqcn = $this->getValueObjectClassName();
        if (is_scalar($value)) {
            $value = $fqcn::create($value);
        }
        if (!is_a($value, $fqcn)) {
            throw ConversionException::conversionFailedFormat($value, $this->getName(), $fqcn);
        }

        return $value->__toString();
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $fqcn = $this->getValueObjectClassName();

        return $fqcn::create($value);
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }

    abstract protected function getValueObjectClassName();
}
