<?php
declare(strict_types=1);

namespace App\Infrastructure\Shared\EventListener;

use App\UserInterface\Api\Exception\ValidatorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

class ValidationExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getException();
        if (!$exception instanceof ValidatorException) {
            return;
        }

        $camelCaseToSnakeCaseNameConverter = new CamelCaseToSnakeCaseNameConverter();
        $convertedList = [];
        foreach ($exception->getMessages() as $fieldName => $messageList) {
            $convertedList[$camelCaseToSnakeCaseNameConverter->normalize($fieldName)] = [];
            foreach ($messageList as $message) {
                $convert = $message;
                $convert['field'] = $camelCaseToSnakeCaseNameConverter->normalize($convert['field']);
                $convertedList[$camelCaseToSnakeCaseNameConverter->normalize($fieldName)][] = $convert;
            }
        }
        $event->setResponse(new JsonResponse($convertedList, Response::HTTP_BAD_REQUEST));
    }
}
