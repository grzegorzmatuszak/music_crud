<?php
declare(strict_types=1);

namespace App\Infrastructure\Shared\EventListener;

use App\UserInterface\Api\Exception\ValidatorException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ExceptionListener
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getException();
        if ($exception instanceof ValidatorException) {
            return;
        }

        $this->logger->error($exception->getMessage(), [
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
            'stacktrace' => $exception->getTraceAsString(),
        ]);
    }
}
