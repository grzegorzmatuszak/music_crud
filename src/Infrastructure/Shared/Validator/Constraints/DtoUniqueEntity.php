<?php

namespace App\Infrastructure\Shared\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @see https://gist.github.com/webbertakken/569409670bfc7c079e276f79260105ed
 */
class DtoUniqueEntity extends Constraint
{
    public const NOT_UNIQUE_ERROR = 'e777db8d-3af0-41f6-8a73-55255375cdca';

    protected static $errorNames = [
        self::NOT_UNIQUE_ERROR => 'NOT_UNIQUE_ERROR',
    ];

    public ?EntityManagerInterface $entityManager = null;

    public $entityClass;

    public $errorPath;

    public array $fieldMapping = [];

    public bool $ignoreNull = true;

    public string $message = 'This value is already used.';

    public string $repositoryMethod = 'findBy';

    public function getDefaultOption()
    {
        return 'entityClass';
    }

    public function getRequiredOptions()
    {
        return ['fieldMapping', 'entityClass'];
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return DtoUniqueEntityValidator::class;
    }
}
