<?php

namespace App\Infrastructure\Shared\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use function get_class;

/**
 * @Annotation
 */
class EntitiesExists extends Constraint
{
    public ?EntityManagerInterface $entityManager = null;

    public string $entityClass = '';

    public bool $ignoreNull = true;

    public string $message = 'Selected fields doesn\'t exists: {{ elements }}';

    public string $repositoryMethod = 'findBy';

    public function getRequiredOptions()
    {
        return 'entityClass';
    }

    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
