<?php

namespace App\Infrastructure\Shared\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NotBlankArrayValidator extends ConstraintValidator
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function validate($dataToValidate, Constraint $constraint)
    {
        if (empty($dataToValidate) || !is_array($dataToValidate)) {
            return;
        }

        if ($this->isEmptyArray($dataToValidate)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }

    public function isEmptyArray(array $dataToValidate): bool
    {
        foreach ($dataToValidate as $elem) {
            if (!empty($elem)) {
                return false;
            }
        }

        return true;
    }
}
