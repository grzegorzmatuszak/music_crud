<?php

namespace App\Infrastructure\Shared\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use function get_class;

/**
 * @Annotation
 */
class NotBlankArray extends Constraint
{
    public ?EntityManagerInterface $entityManager = null;

    public bool $ignoreNull = true;

    public string $message = 'This value should not be blank.';

    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
