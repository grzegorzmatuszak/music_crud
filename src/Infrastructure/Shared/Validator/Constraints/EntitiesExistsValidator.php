<?php

namespace App\Infrastructure\Shared\Validator\Constraints;

use App\Application\VO\UuidVO;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function count;

class EntitiesExistsValidator extends ConstraintValidator
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function validate($dataToValidate, Constraint $constraint)
    {
        if (empty($dataToValidate) || (is_array($dataToValidate) && $this->isEmptyArray($dataToValidate))) {
            return;
        }

        if (is_scalar($dataToValidate)) {
            $dataToValidate = [$dataToValidate];
        }

        if (!is_array($dataToValidate)) {
            throw new UnexpectedTypeException($dataToValidate, 'array');
        }

        $dataToValidate = array_values(array_unique($dataToValidate));
        $resultList = $this->entityManager->getRepository($constraint->entityClass)->findBy(['id' => $dataToValidate]);

        if (count($resultList) === count($dataToValidate)) {
            return;
        }

        $missedIds = array_flip($dataToValidate);
        foreach ($resultList as $result) {
            if ($result->getId() instanceof UuidVO) {
                if (isset($missedIds[$result->getId()->getId()])) {
                    unset($missedIds[$result->getId()->getId()]);
                }
            } else {
                if (isset($missedIds[$result->getId()])) {
                    unset($missedIds[$result->getId()]);
                }
            }
        }
        $missedIds = implode(', ', array_flip($missedIds));
        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ elements }}', $missedIds)
            ->addViolation();
    }

    private function isEmptyArray(array $dataToValidate): bool
    {
        foreach ($dataToValidate as $elem) {
            if (!empty($elem)) {
                return false;
            }
        }

        return true;
    }
}
