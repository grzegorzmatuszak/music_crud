<?php
declare(strict_types=1);

namespace App\Infrastructure\Artist\ORM\Type;

use App\Domain\Artist\Entity\VO\ArtistIdVO;
use App\Infrastructure\Shared\ORM\Type\AbstractIdType;

class ArtistIdType extends AbstractIdType
{
    const ARTIST_ID = 'artist_id';

    public function getName()
    {
        return static::ARTIST_ID;
    }

    protected function getValueObjectClassName(): string
    {
        return ArtistIdVO::class;
    }
}
