<?php
declare(strict_types=1);

namespace App\Infrastructure\Artist\Query;

use App\Application\Query\V1\Artist\Model\ArtistDetailsModel;
use App\Application\Query\V1\Artist\Query\ArtistDetailsQueryInterface;
use App\Domain\Artist\Entity\Artist;
use Doctrine\ORM\EntityManagerInterface;

final class ArtistDetailsQuery implements ArtistDetailsQueryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $id
     * @param bool   $active
     *
     * @return ArtistDetailsModel|null
     */
    public function __invoke(string $id, bool $active = true): ?ArtistDetailsModel
    {
        $query = $this->entityManager->createQueryBuilder();
        $query
            ->select('a.id, a.name.firstName as firstName, a.name.lastName as lastName, a.nick.nick as nick')
            ->from(Artist::class, 'a')
            ->andWhere('a.id = :id')
            ->setParameter('id', $id);

        if ($active) {
            $query
                ->andWhere('a.active = :active')
                ->setParameter('active', true);
        }

        $result = $query->getQuery()->getResult();
        if (empty($result)) {
            return null;
        }
        $result = reset($result);

        return new ArtistDetailsModel($result['id']->getId(), $result['firstName'], $result['lastName'], $result['nick']);
    }
}
