<?php
declare(strict_types=1);

namespace App\Infrastructure\Artist\Query;

use App\Application\Query\V1\Artist\Model\ArtistSongModel;
use App\Application\Query\V1\Artist\Model\ArtistSongsModel;
use App\Application\Query\V1\Artist\Query\ArtistSongsQueryInterface;
use App\Domain\Artist\Entity\Artist;
use App\Domain\Song\Entity\VO\SongIdVO;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

class ArtistSongsQuery implements ArtistSongsQueryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(string $id, bool $active = true): ?ArtistSongsModel
    {
        $query = $this->entityManager->createQueryBuilder();

        $query
            ->select('a.id as artistId, s.id as songId, s.title.title as title, s.genre.genre as genre, s.ismn.ismn as ismn, s.date.year as year')
            ->from(Artist::class, 'a')
            ->leftJoin('a.songs', 's', 'WITH', 's.active = :active');
        if ($active) {
            $query->andWhere('a.active = :active');
            $query->setParameter('active', true);
        }
        $query->andWhere('a.id = :id');
        $query->setParameter('id', $id);

        $resultList = $query->getQuery()->getResult();

        if (empty($resultList)) {
            return null;
        }

        $songCollection = new ArrayCollection();

        foreach ($resultList as $result) {
            if ($result['songId'] instanceof SongIdVO && !is_null($result['songId']->getId())) {
                $songCollection->add(new ArtistSongModel($result['songId']->getId(), $result['title'], $result['genre'], $result['ismn'], (int) $result['year']));
            }
        }

        return new ArtistSongsModel(reset($resultList)['artistId']->getId(), $songCollection);
    }
}
