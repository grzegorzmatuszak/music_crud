<?php
declare(strict_types=1);

namespace App\Infrastructure\Artist\Query;

use App\Application\Query\V1\Artist\Model\ArtistListModel;
use App\Application\Query\V1\Artist\Query\ArtistListQueryInterface;
use App\Domain\Artist\Entity\Artist;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

final class ArtistListQuery implements ArtistListQueryInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function __invoke(bool $active = true): ArrayCollection
    {
        $query = $this->entityManager->createQueryBuilder();
        $query
            ->select('a.id, a.name.firstName as firstName, a.name.lastName as lastName, a.nick.nick as nick')
            ->from(Artist::class, 'a');

        if ($active) {
            $query
                ->andWhere('a.active = :active')
                ->setParameter('active', true);
        }
        $resultList = $query->getQuery()->getResult();

        $collectionResult = new ArrayCollection();
        foreach ($resultList as $result) {
            $collectionResult->add(new ArtistListModel($result['id']->getId(), $result['firstName'], $result['lastName'], $result['nick']));
        }

        return $collectionResult;
    }
}
