<?php
declare(strict_types=1);

namespace App\Infrastructure\Artist\Repository;

use App\Domain\Artist\Entity\Artist;
use App\Domain\Artist\Repository\GetArtistRepositoryInterface;
use App\Domain\Shared\Exception\NotFoundException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class GetArtistRepository extends ServiceEntityRepository implements GetArtistRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Artist::class);
    }

    public function findById(string $id, bool $active = true): Artist
    {
        $qb = $this->createQueryBuilder('a')
            ->andWhere('a.id = :id')
            ->setParameter('id', $id);

        if ($active) {
            $qb->andWhere('a.active = :active');
            $qb->setParameter('active', true);
        }

        $result = $qb
            ->getQuery()
            ->getResult();

        if (empty($result)) {
            throw new NotFoundException('Artist not found');
        }

        return reset($result);
    }

    public function findByIds(array $idList, bool $active = true): array
    {
        $qb = $this->createQueryBuilder('a')
            ->andWhere('a.id IN (:idList)')
            ->setParameter('idList', $idList);

        if ($active) {
            $qb->andWhere('a.active = :active');
            $qb->setParameter('active', true);
        }

        return $qb
            ->getQuery()
            ->getResult();
    }
}
