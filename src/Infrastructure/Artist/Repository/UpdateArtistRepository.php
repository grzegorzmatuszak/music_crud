<?php
declare(strict_types=1);

namespace App\Infrastructure\Artist\Repository;

use App\Domain\Artist\Entity\VO\ArtistNameVO;
use App\Domain\Artist\Entity\VO\ArtistNickVO;
use App\Domain\Artist\Repository\GetArtistRepositoryInterface;
use App\Domain\Artist\Repository\UpdateArtistRepositoryInterface;
use App\Domain\Song\Repository\GetSongRepositoryInterface;

final class UpdateArtistRepository implements UpdateArtistRepositoryInterface
{
    private GetArtistRepositoryInterface $getArtistRepository;

    private GetSongRepositoryInterface $getSongRepository;

    public function __construct(GetArtistRepositoryInterface $getArtistRepository, GetSongRepositoryInterface $getSongRepository)
    {
        $this->getArtistRepository = $getArtistRepository;
        $this->getSongRepository = $getSongRepository;
    }

    public function deleteArtist(string $id): void
    {
        $artist = $this->getArtistRepository->findById($id);
        $artist->deactivate();
    }

    public function update(
        string $id,
        string $firstName,
        string $lastName,
        string $nick,
        array $songsIds = []
    ): void {
        $artist = $this->getArtistRepository->findById($id);

        if (!empty($songsIds)) {
            $resultList = $this->getSongRepository->findByIds($songsIds);
            foreach ($resultList as $result) {
                $artist->addSong($result);
            }
        }
        $artist->setName(ArtistNameVO::create($firstName, $lastName));
        $artist->setNick(ArtistNickVO::create($nick));
    }
}
