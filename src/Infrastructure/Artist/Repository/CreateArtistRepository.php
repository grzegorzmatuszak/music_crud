<?php
declare(strict_types=1);

namespace App\Infrastructure\Artist\Repository;

use App\Domain\Artist\Entity\Artist;
use App\Domain\Artist\Entity\VO\ArtistIdVO;
use App\Domain\Artist\Entity\VO\ArtistNameVO;
use App\Domain\Artist\Entity\VO\ArtistNickVO;
use App\Domain\Artist\Repository\CreateArtistRepositoryInterface;
use App\Domain\Song\Repository\GetSongRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

final class CreateArtistRepository implements CreateArtistRepositoryInterface
{
    private EntityManagerInterface $entityManager;

    private GetSongRepositoryInterface $getSongRepository;

    public function __construct(EntityManagerInterface $entityManager, GetSongRepositoryInterface $getSongRepository)
    {
        $this->entityManager = $entityManager;
        $this->getSongRepository = $getSongRepository;
    }

    public function save(
        string $id,
        string $firstName,
        string $lastName,
        string $nick,
        array $songsIds = []
    ): void {
        $artist = Artist::create(ArtistIdVO::create($id), ArtistNameVO::create($firstName, $lastName), ArtistNickVO::create($nick));

        if (!empty($songsIds)) {
            $resultList = $this->getSongRepository->findByIds($songsIds);
            foreach ($resultList as $result) {
                $artist->addSong($result);
            }
        }

        $this->entityManager->persist($artist);
    }
}
