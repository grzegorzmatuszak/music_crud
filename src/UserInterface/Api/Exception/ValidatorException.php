<?php
declare(strict_types=1);

namespace App\UserInterface\Api\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Exception\ValidatorException as BaseValidatorException;
use Throwable;

class ValidatorException extends BaseValidatorException
{
    private ConstraintViolationListInterface $errors;

    public function __construct(
        ConstraintViolationListInterface $errorList,
        $code = Response::HTTP_BAD_REQUEST,
        Throwable $previous = null
    ) {
        $this->errors = $errorList;
        parent::__construct('Validator error', $code, $previous);
    }

    public function getMessages(): array
    {
        $messageList = [];
        /** @var ConstraintViolation $violationList */
        foreach ($this->errors as $paramName => $violationList) {
            $messageList[$violationList->getPropertyPath()][] = [
                'field' => $violationList->getPropertyPath(),
                'message' => $violationList->getMessage(),
            ];
        }

        return $messageList;
    }
}
