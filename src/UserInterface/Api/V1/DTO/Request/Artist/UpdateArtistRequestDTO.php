<?php
declare(strict_types=1);

namespace App\UserInterface\Api\V1\DTO\Request\Artist;

use App\Infrastructure\Shared\Validator\Constraints\EntitiesExists;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class UpdateArtistRequestDTO
{
    /**
     * @Assert\Length(min="2",max="100")
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    private ?string $firstName = null;

    /**
     * @Assert\Length(min="2",max="100")
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    private ?string $lastName = null;

    /**
     * @Assert\Length(min="1",max="255")
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    private ?string $nick = null;

    /**
     * @Serializer\Type("array")
     * @EntitiesExists(
     *     entityClass="App\Domain\Song\Entity\Song",
     * )
     */
    private ?array $songsIds = [];

    public function __construct(string $firstName, string $lastName, string $nick, array $songsIds = [])
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->nick = $nick;
        $this->songsIds = $songsIds;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getNick(): string
    {
        return $this->nick;
    }

    public function getSongsIds(): array
    {
        return $this->songsIds;
    }
}
