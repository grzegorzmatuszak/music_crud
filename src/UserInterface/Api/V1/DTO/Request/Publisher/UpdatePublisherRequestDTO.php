<?php
declare(strict_types=1);

namespace App\UserInterface\Api\V1\DTO\Request\Publisher;

use App\Infrastructure\Shared\Validator\Constraints\EntitiesExists;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class UpdatePublisherRequestDTO
{
    /**
     * @Assert\Length(min="2",max="255")
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    private ?string $name = null;

    /**
     * @Assert\Length(min="2",max="255")
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    private ?string $nip = null;

    /**
     * @Assert\Length(min="2",max="255")
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    private ?string $address = null;

    /**
     * @Serializer\Type("array")
     * @EntitiesExists(
     *     entityClass="App\Domain\Song\Entity\Song",
     * )
     */
    private ?array $songsIds = [];

    public function __construct(string $name, string $nip, string $address, array $songsIds = [])
    {
        $this->songsIds = $songsIds;
        $this->name = $name;
        $this->nip = $nip;
        $this->address = $address;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getNip(): string
    {
        return $this->nip;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getSongsIds(): array
    {
        return $this->songsIds;
    }
}
