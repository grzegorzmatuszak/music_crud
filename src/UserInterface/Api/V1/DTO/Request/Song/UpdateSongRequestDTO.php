<?php
declare(strict_types=1);

namespace App\UserInterface\Api\V1\DTO\Request\Song;

use App\Infrastructure\Shared\Validator\Constraints\EntitiesExists;
use App\Infrastructure\Shared\Validator\Constraints\NotBlankArray;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class UpdateSongRequestDTO
{
    /**
     * @Assert\Length(min="2",max="100")
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    private ?string $title = null;

    /**
     * @Assert\Length(min="2",max="100")
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    private ?string $genre = null;

    /**
     * @Assert\Length(min="2",max="255")
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     */
    private ?string $ismn = null;

    /**
     * @Assert\Length(min="4",max="4")
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Positive
     * @Serializer\Type("integer")
     */
    private ?int $year = null;

    /**
     * @Assert\NotBlank()
     * @NotBlankArray()
     * @Serializer\Type("array")
     * @EntitiesExists(
     *     entityClass="App\Domain\Artist\Entity\Artist",
     * )
     */
    private ?array $artistsIds = [];

    /**
     * @Assert\NotBlank()
     * @Serializer\Type("string")
     * @EntitiesExists(
     *     entityClass="App\Domain\Publisher\Entity\Publisher",
     * )
     */
    private ?string $publisherId = null;

    public function __construct(
        string $title,
        string $genre,
        string $ismn,
        string $publisherId,
        int $year,
        array $artistsIds = []
    ) {
        $this->artistsIds = $artistsIds;
        $this->title = $title;
        $this->genre = $genre;
        $this->ismn = $ismn;
        $this->year = $year;
        $this->publisherId = $publisherId;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getGenre(): string
    {
        return $this->genre;
    }

    public function getIsmn(): string
    {
        return $this->ismn;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getArtistsIds(): array
    {
        return $this->artistsIds;
    }

    public function getPublisherId(): string
    {
        return $this->publisherId;
    }
}
