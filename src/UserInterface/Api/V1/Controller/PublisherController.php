<?php
declare(strict_types=1);

namespace App\UserInterface\Api\V1\Controller;

use App\Application\Command\Publisher\Create\CreatePublisherCommand;
use App\Application\Command\Publisher\Delete\DeletePublisherCommand;
use App\Application\Command\Publisher\Update\UpdatePublisherCommand;
use App\Application\Query\Shared\Model\IdModel;
use App\Application\Query\V1\Publisher\Model\PublisherDetailsModel;
use App\Application\Query\V1\Publisher\Model\PublisherSongsModel;
use App\Application\Query\V1\Publisher\Query\PublisherDetailsQueryInterface;
use App\Application\Query\V1\Publisher\Query\PublisherListQueryInterface;
use App\Application\Query\V1\Publisher\Query\PublisherSongsQueryInterface;
use App\Application\Service\Generator\UniqId\UuidV4GeneratorInterface;
use App\Domain\Publisher\Repository\GetPublisherRepositoryInterface;
use App\UserInterface\Api\Exception\ValidatorException;
use App\UserInterface\Api\V1\DTO\Request\Publisher\CreatePublisherRequestDTO;
use App\UserInterface\Api\V1\DTO\Request\Publisher\UpdatePublisherRequestDTO;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class PublisherController extends AbstractFOSRestController
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * @Rest\View(statusCode=200)
     * @SWG\Response(
     *     response=200,
     *     description="Returns publishers list",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(type="object", ref=@Model(type=PublisherListModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @Rest\Get("/publishers", name="api_get_publishers")
     * @param PublisherListQueryInterface $publisherListQuery
     *
     * @return View
     */
    public function index(PublisherListQueryInterface $publisherListQuery): View
    {
        return $this->view($publisherListQuery());
    }

    /**
     * @Rest\View(statusCode=200)
     * @SWG\Response(
     *     response=200,
     *     description="Publisher details",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=PublisherDetailsModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Resource not found",
     * )
     * @Rest\Get("/publisher/{id}", name="api_get_publisher")
     * @param string                         $id
     * @param PublisherDetailsQueryInterface $publisherDetailsQuery
     *
     * @return View
     */
    public function getPublisher(string $id, PublisherDetailsQueryInterface $publisherDetailsQuery): View
    {
        $result = $publisherDetailsQuery($id);

        if ($result instanceof PublisherDetailsModel) {
            return $this->view($result);
        }

        throw new HttpException(Response::HTTP_NOT_FOUND, 'Not found');
    }

    /**
     * @Rest\View(statusCode=201)
     * @SWG\Response(
     *     response=201,
     *     description="Add publisher",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=IdModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     *     )
     * @SWG\Response(
     *     response=400,
     *     description="invalid",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="query",
     *     description="Name",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="nip",
     *     in="query",
     *     description="nip",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="address",
     *     in="query",
     *     description="address",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="songs_ids",
     *     in="query",
     *     type="string",
     *     required=false,
     *     @SWG\Items(type="string"),
     *     collectionFormat="multi"
     * )
     * @Rest\Post("/publisher", name="api_post_publisher")
     * @param CreatePublisherRequestDTO $createPublisherRequestDTO
     * @param UuidV4GeneratorInterface  $uniqIdGenerator
     * @param ValidatorInterface        $validator
     * @ParamConverter("createPublisherRequestDTO", converter="fos_rest.request_body")
     *
     * @return View
     */
    public function postPublisher(
        CreatePublisherRequestDTO $createPublisherRequestDTO,
        UuidV4GeneratorInterface $uniqIdGenerator,
        ValidatorInterface $validator
    ): View {
        $id = $uniqIdGenerator->generate();

        $errorList = $validator->validate($createPublisherRequestDTO);
        if (\count($errorList) > 0) {
            throw new ValidatorException($errorList);
        }

        $message = new CreatePublisherCommand(
            $id,
            $createPublisherRequestDTO->getName(),
            $createPublisherRequestDTO->getNip(),
            $createPublisherRequestDTO->getAddress(),
            $createPublisherRequestDTO->getSongsIds()
        );

        $this->messageBus->dispatch($message);

        return $this->view(new IdModel($id));
    }

    /**
     * @Rest\View(statusCode=201)
     * @SWG\Response(
     *     response=201,
     *     description="Edit publisher",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=IdModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=400,
     *     description="invalid",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Resource not found",
     * )
     * @SWG\Parameter(
     *     name="name",
     *     in="query",
     *     description="Name",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="nip",
     *     in="query",
     *     description="nip",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="address",
     *     in="query",
     *     description="address",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="songs_ids",
     *     in="query",
     *     type="string",
     *     required=false,
     *     @SWG\Items(type="string"),
     *     collectionFormat="multi"
     * )
     * @Rest\Put("/publisher/{id}", name="api_put_publisher")
     * @param string                          $id
     * @param UpdatePublisherRequestDTO       $updatePublisherRequestDTO
     * @param ValidatorInterface              $validator
     * @param GetPublisherRepositoryInterface $getPublisherRepository
     * @ParamConverter("updatePublisherRequestDTO", converter="fos_rest.request_body")
     *
     * @return View
     */
    public function putPublisher(
        string $id,
        UpdatePublisherRequestDTO $updatePublisherRequestDTO,
        ValidatorInterface $validator,
        GetPublisherRepositoryInterface $getPublisherRepository
    ): View {
        $errorList = $validator->validate($updatePublisherRequestDTO);
        if (\count($errorList) > 0) {
            throw new ValidatorException($errorList);
        }

        $message = new UpdatePublisherCommand(
            $id,
            $updatePublisherRequestDTO->getName(),
            $updatePublisherRequestDTO->getNip(),
            $updatePublisherRequestDTO->getAddress(),
            $updatePublisherRequestDTO->getSongsIds()
        );

        $getPublisherRepository->findById($message->getId());

        $this->messageBus->dispatch($message);

        return $this->view(new IdModel($id));
    }

    /**
     * @Rest\View(statusCode=201)
     * @SWG\Response(
     *     response=201,
     *     description="Edit publisher",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=IdModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=400,
     *     description="invalid",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Resource not found",
     * )
     * @Rest\Delete("/publisher/{id}", name="api_delete_publisher")
     * @param string                 $id
     * @param GetPublisherRepositoryInterface $publisherRepository
     *
     * @return View
     */
    public function deletePublisher(
        string $id,
        GetPublisherRepositoryInterface $publisherRepository
    ): View {
        $publisherRepository->findById($id);

        $this->messageBus->dispatch(new DeletePublisherCommand($id));

        return $this->view();
    }

    /**
     * @Rest\View(statusCode=200)
     * @SWG\Response(
     *     response=200,
     *     description="Returns publisher's song list",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(type="object", ref=@Model(type=PublisherSongListModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @Rest\Get("/publisher/{id}/songs", name="api_get_publisher_songs")
     * @param string                       $id
     * @param PublisherSongsQueryInterface $publisherSongsQuery
     *
     * @return View
     */
    public function publisherSongs(string $id, PublisherSongsQueryInterface $publisherSongsQuery): View
    {
        $result = $publisherSongsQuery($id);
        if ($result instanceof PublisherSongsModel) {
            return $this->view($result);
        }

        throw new HttpException(Response::HTTP_NOT_FOUND, 'Not found');
    }
}
