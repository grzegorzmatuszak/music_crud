<?php
declare(strict_types=1);

namespace App\UserInterface\Api\V1\Controller;

use App\Application\Command\Song\Create\CreateSongCommand;
use App\Application\Command\Song\Delete\DeleteSongCommand;
use App\Application\Command\Song\Update\UpdateSongCommand;
use App\Application\Query\Shared\Model\IdModel;
use App\Application\Query\V1\Song\Model\SongArtistsModel;
use App\Application\Query\V1\Song\Model\SongDetailsModel;
use App\Application\Query\V1\Song\Query\SongArtistsQueryInterface;
use App\Application\Query\V1\Song\Query\SongDetailsQueryInterface;
use App\Application\Query\V1\Song\Query\SongListQueryInterface;
use App\Application\Service\Generator\UniqId\UuidV4GeneratorInterface;
use App\Domain\Song\Repository\GetSongRepositoryInterface;
use App\UserInterface\Api\Exception\ValidatorException;
use App\UserInterface\Api\V1\DTO\Request\Song\CreateSongRequestDTO;
use App\UserInterface\Api\V1\DTO\Request\Song\UpdateSongRequestDTO;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class SongController extends AbstractFOSRestController
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * @Rest\View(statusCode=200)
     * @SWG\Response(
     *     response=200,
     *     description="Returns songs list",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(type="object", ref=@Model(type=SongListModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @Rest\Get("/songs", name="api_get_songs")
     * @param SongListQueryInterface $songListQuery
     *
     * @return View
     */
    public function index(SongListQueryInterface $songListQuery): View
    {
        return $this->view($songListQuery());
    }

    /**
     * @Rest\View(statusCode=200)
     * @SWG\Response(
     *     response=200,
     *     description="Song details",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=SongDetailsModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Resource not found",
     * )
     * @Rest\Get("/song/{id}", name="api_get_song")
     * @param string                    $id
     * @param SongDetailsQueryInterface $songDetailsQuery
     *
     * @return View
     */
    public function getSong(string $id, SongDetailsQueryInterface $songDetailsQuery): View
    {
        $result = $songDetailsQuery($id);

        if ($result instanceof SongDetailsModel) {
            return $this->view($result);
        }

        throw new HttpException(Response::HTTP_NOT_FOUND, 'Not found');
    }

    /**
     * @Rest\View(statusCode=201)
     * @SWG\Response(
     *     response=201,
     *     description="Add song",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=IdModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="invalid",
     *     )
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     *     )
     * )
     * @SWG\Parameter(
     *     name="title",
     *     in="query",
     *     description="Title",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="genre",
     *     in="query",
     *     description="Genre",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="ismn",
     *     in="query",
     *     description="Ismn",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="publisher_id",
     *     in="query",
     *     description="Publisher id",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="nip",
     *     in="query",
     *     description="Nip",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="artists_ids",
     *     in="query",
     *     type="string",
     *     required=true,
     *     @SWG\Items(type="string"),
     *     collectionFormat="multi"
     * )
     * @Rest\Post("/song", name="api_post_song")
     * @param CreateSongRequestDTO     $createSongRequestDTO
     * @param UuidV4GeneratorInterface $uniqIdGenerator
     * @param ValidatorInterface       $validator
     * @ParamConverter("createSongRequestDTO", converter="fos_rest.request_body")
     *
     * @return View
     */
    public function postSong(
        CreateSongRequestDTO $createSongRequestDTO,
        UuidV4GeneratorInterface $uniqIdGenerator,
        ValidatorInterface $validator
    ): View {
        $id = $uniqIdGenerator->generate();

        $errorList = $validator->validate($createSongRequestDTO);
        if (\count($errorList) > 0) {
            throw new ValidatorException($errorList);
        }

        $message = new CreateSongCommand(
            $id,
            $createSongRequestDTO->getTitle(),
            $createSongRequestDTO->getGenre(),
            $createSongRequestDTO->getIsmn(),
            $createSongRequestDTO->getYear(),
            $createSongRequestDTO->getPublisherId(),
            $createSongRequestDTO->getArtistsIds(),
        );

        $this->messageBus->dispatch($message);

        return $this->view(new IdModel($id));
    }

    /**
     * @Rest\View(statusCode=201)
     * @SWG\Response(
     *     response=201,
     *     description="Edit song",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=IdModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="invalid",
     *     )
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     *     )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Resource not found",
     *     )
     * )
     * @SWG\Parameter(
     *     name="title",
     *     in="query",
     *     description="Title",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="genre",
     *     in="query",
     *     description="Genre",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="ismn",
     *     in="query",
     *     description="Ismn",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="publisher_id",
     *     in="query",
     *     description="Publisher id",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="publisher_id",
     *     in="query",
     *     description="Publisher id",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="nip",
     *     in="query",
     *     description="Nip",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="artists_ids",
     *     in="query",
     *     type="string",
     *     required=true,
     *     @SWG\Items(type="string"),
     *     collectionFormat="multi"
     * )
     * @Rest\Put("/song/{id}", name="api_put_song")
     * @param string                     $id
     * @param UpdateSongRequestDTO       $updateSongRequestDTO
     * @param ValidatorInterface         $validator
     * @param GetSongRepositoryInterface $getSongRepository
     * @ParamConverter("updateSongRequestDTO", converter="fos_rest.request_body")
     *
     * @return View
     */
    public function putSong(
        string $id,
        UpdateSongRequestDTO $updateSongRequestDTO,
        ValidatorInterface $validator,
        GetSongRepositoryInterface $getSongRepository
    ): View {
        $errorList = $validator->validate($updateSongRequestDTO);
        if (\count($errorList) > 0) {
            throw new ValidatorException($errorList);
        }

        $message = new UpdateSongCommand(
            $id,
            $updateSongRequestDTO->getTitle(),
            $updateSongRequestDTO->getGenre(),
            $updateSongRequestDTO->getIsmn(),
            $updateSongRequestDTO->getYear(),
            $updateSongRequestDTO->getPublisherId(),
            $updateSongRequestDTO->getArtistsIds()
        );

        $getSongRepository->findById($message->getId());

        $this->messageBus->dispatch($message);

        return $this->view(new IdModel($id));
    }

    /**
     * @Rest\View(statusCode=201)
     * @SWG\Response(
     *     response=201,
     *     description="Edit song",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=IdModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=400,
     *     description="invalid",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Resource not found",
     * )
     * @Rest\Delete("/song/{id}", name="api_delete_song")
     * @param string                     $id
     * @param GetSongRepositoryInterface $songRepository
     *
     * @return View
     */
    public function deleteSong(
        string $id,
        GetSongRepositoryInterface $songRepository
    ): View {
        $songRepository->findById($id);
        $this->messageBus->dispatch(new DeleteSongCommand($id));

        return $this->view();
    }

    /**
     * @Rest\View(statusCode=200)
     * @SWG\Response(
     *     response=200,
     *     description="Returns artist's song list",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(type="object", ref=@Model(type=SongArtistsListModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Resource not found",
     * )
     * @Rest\Get("/song/{id}/artists", name="api_get_song_artists")
     * @param string                    $id
     * @param SongArtistsQueryInterface $songArtistsQuery
     *
     * @return View
     */
    public function songArtists(string $id, SongArtistsQueryInterface $songArtistsQuery): View
    {
        $result = $songArtistsQuery($id);
        if ($result instanceof SongArtistsModel) {
            return $this->view($result);
        }

        throw new HttpException(Response::HTTP_NOT_FOUND, 'Not found');
    }
}
