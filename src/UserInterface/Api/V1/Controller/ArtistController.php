<?php
declare(strict_types=1);

namespace App\UserInterface\Api\V1\Controller;

use App\Application\Command\Artist\Create\CreateArtistCommand;
use App\Application\Command\Artist\Delete\DeleteArtistCommand;
use App\Application\Command\Artist\Update\UpdateArtistCommand;
use App\Application\Query\Shared\Model\IdModel;
use App\Application\Query\V1\Artist\Model\ArtistDetailsModel;
use App\Application\Query\V1\Artist\Model\ArtistSongsModel;
use App\Application\Query\V1\Artist\Query\ArtistDetailsQueryInterface;
use App\Application\Query\V1\Artist\Query\ArtistListQueryInterface;
use App\Application\Query\V1\Artist\Query\ArtistSongsQueryInterface;
use App\Application\Service\Generator\UniqId\UuidV4GeneratorInterface;
use App\Domain\Artist\Repository\GetArtistRepositoryInterface;
use App\UserInterface\Api\Exception\ValidatorException;
use App\UserInterface\Api\V1\DTO\Request\Artist\CreateArtistRequestDTO;
use App\UserInterface\Api\V1\DTO\Request\Artist\UpdateArtistRequestDTO;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function count;

final class ArtistController extends AbstractFOSRestController
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * @Rest\View(statusCode=200)
     * @SWG\Response(
     *     response=200,
     *     description="Returns artists list",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(type="object", ref=@Model(type=ArtistListModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     *
     * @Rest\Get("/artists", name="api_get_artists")
     * @param ArtistListQueryInterface $artistListQuery
     *
     * @return View
     */
    public function index(ArtistListQueryInterface $artistListQuery): View
    {
        return $this->view($artistListQuery());
    }

    /**
     * @Rest\View(statusCode=200)
     * @SWG\Response(
     *     response=200,
     *     description="Artist details",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=ArtistDetailsModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Resource not found",
     * )
     *
     * @Rest\Get("/artist/{id}", name="api_get_artist")
     * @param string                      $id
     * @param ArtistDetailsQueryInterface $artistDetailsQuery
     *
     * @return View
     */
    public function getArtist(string $id, ArtistDetailsQueryInterface $artistDetailsQuery): View
    {
        $result = $artistDetailsQuery($id);

        if ($result instanceof ArtistDetailsModel) {
            return $this->view($result);
        }

        throw new HttpException(Response::HTTP_NOT_FOUND, 'Not found');
    }

    /**
     * @Rest\View(statusCode=201)
     * @SWG\Response(
     *     response=201,
     *     description="Add artist",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=IdModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=400,
     *     description="invalid",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @SWG\Parameter(
     *     name="first_name",
     *     in="query",
     *     description="First name",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="last_name",
     *     in="query",
     *     description="Last name",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="nick",
     *     in="query",
     *     description="Nick",
     *     required=true,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="songs_ids",
     *     in="query",
     *     type="string",
     *     required=false,
     *     @SWG\Items(type="string"),
     *     collectionFormat="multi"
     * )
     *
     * @Rest\Post("/artist", name="api_post_artist")
     * @param CreateArtistRequestDTO   $createArtistRequestDTO
     * @param UuidV4GeneratorInterface $uniqIdGenerator
     * @param ValidatorInterface       $validator
     * @ParamConverter("createArtistRequestDTO", converter="fos_rest.request_body")
     *
     * @return View
     */
    public function postArtist(
        CreateArtistRequestDTO $createArtistRequestDTO,
        UuidV4GeneratorInterface $uniqIdGenerator,
        ValidatorInterface $validator
    ): View {
        $errorList = $validator->validate($createArtistRequestDTO);
        if (count($errorList) > 0) {
            throw new ValidatorException($errorList);
        }
        $id = $uniqIdGenerator->generate();
        $message = new CreateArtistCommand(
            $id,
            $createArtistRequestDTO->getFirstName(),
            $createArtistRequestDTO->getLastName(),
            $createArtistRequestDTO->getNick(),
            $createArtistRequestDTO->getSongsIds()
        );

        $this->messageBus->dispatch($message);

        return $this->view(new IdModel($id));
    }

    /**
     * @Rest\View(statusCode=201)
     * @SWG\Response(
     *     response=201,
     *     description="Edit artist",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=IdModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=400,
     *     description="invalid",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Resource not found",
     * )
     * @SWG\Parameter(
     *     name="first_name",
     *     in="query",
     *     description="First name",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="last_name",
     *     in="query",
     *     description="Last name",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="nick",
     *     in="query",
     *     description="Nick",
     *     required=true,
     *     type="string"
     * )
     * @SWG\Parameter(
     *     name="songs_ids",
     *     in="query",
     *     type="string",
     *     required=false,
     *     @SWG\Items(type="string"),
     *     collectionFormat="multi"
     * )
     *
     * @Rest\Put("/artist/{id}", name="api_put_artist")
     * @param string                 $id
     * @param UpdateArtistRequestDTO $updateArtistRequestDTO
     * @param ValidatorInterface     $validator
     * @param GetArtistRepositoryInterface    $getArtistRepository
     * @ParamConverter("updateArtistRequestDTO", converter="fos_rest.request_body")
     *
     * @return View
     */
    public function putArtist(
        string $id,
        UpdateArtistRequestDTO $updateArtistRequestDTO,
        ValidatorInterface $validator,
        GetArtistRepositoryInterface $getArtistRepository
    ): View {
        $errorList = $validator->validate($updateArtistRequestDTO);

        if (count($errorList) > 0) {
            throw new ValidatorException($errorList);
        }
        $message = new UpdateArtistCommand(
            $id,
            $updateArtistRequestDTO->getFirstName(),
            $updateArtistRequestDTO->getLastName(),
            $updateArtistRequestDTO->getNick(),
            $updateArtistRequestDTO->getSongsIds(),
        );

        $getArtistRepository->findById($message->getId());

        $this->messageBus->dispatch($message);

        return $this->view(new IdModel($id));
    }

    /**
     * @Rest\View(statusCode=201)
     * @SWG\Response(
     *     response=201,
     *     description="Edit artist",
     *     @SWG\Schema(
     *         type="object",
     *         @SWG\Items(ref=@Model(type=IdModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=400,
     *     description="invalid",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Resource not found",
     * )
     *
     * @Rest\Delete("/artist/{id}", name="api_delete_artist")
     * @param string              $id
     * @param GetArtistRepositoryInterface $artistRepository
     *
     * @return View
     */
    public function deleteArtist(
        string $id,
        GetArtistRepositoryInterface $artistRepository
    ): View {
        $artistRepository->findById($id);

        $this->messageBus->dispatch(new DeleteArtistCommand($id));

        return $this->view();
    }

    /**
     * @Rest\View(statusCode=200)
     * @SWG\Response(
     *     response=200,
     *     description="Returns artist's song list",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(type="object", ref=@Model(type=ArtistSongsModel::class))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="unauthorized",
     * )
     * @SWG\Response(
     *     response=403,
     *     description="forbidden",
     * )
     *
     * @Rest\Get("/artist/{id}/songs", name="api_get_artist_songs")
     * @param string                    $id
     * @param ArtistSongsQueryInterface $artistSongsQuery
     *
     * @return View
     */
    public function artistSongs(string $id, ArtistSongsQueryInterface $artistSongsQuery): View
    {
        $result = $artistSongsQuery($id);
        if ($result instanceof ArtistSongsModel) {
            return $this->view($result);
        }

        throw new HttpException(Response::HTTP_NOT_FOUND, 'Not found');
    }
}
