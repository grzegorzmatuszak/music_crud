<?php
declare(strict_types=1);

namespace App\Domain\Shared\Exception;

use RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class NotFoundException extends RuntimeException
{
    public function __construct(
        string $message,
        $code = Response::HTTP_NOT_FOUND,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
