<?php
declare(strict_types=1);

namespace App\Domain\Publisher\Exception;

use InvalidArgumentException;

class PublisherInvalidArgumentException extends InvalidArgumentException
{
}
