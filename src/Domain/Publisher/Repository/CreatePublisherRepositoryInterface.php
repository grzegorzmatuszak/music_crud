<?php
declare(strict_types=1);

namespace App\Domain\Publisher\Repository;

interface CreatePublisherRepositoryInterface
{
    public function save(
        string $id,
        string $name,
        string $nip,
        string $address,
        array $songsIds = []
    ): void;
}
