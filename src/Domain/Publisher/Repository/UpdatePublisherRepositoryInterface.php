<?php
declare(strict_types=1);

namespace App\Domain\Publisher\Repository;

interface UpdatePublisherRepositoryInterface
{
    public function deletePublisher(string $id): void;

    public function update(
        string $id,
        string $name,
        string $nip,
        string $address,
        array $songsIds = []
    ): void;
}
