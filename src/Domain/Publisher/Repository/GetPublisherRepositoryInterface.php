<?php
declare(strict_types=1);

namespace App\Domain\Publisher\Repository;

use App\Domain\Publisher\Entity\Publisher;

interface GetPublisherRepositoryInterface
{
    public function findById(string $id, bool $active = true): Publisher;

    public function findByIds(array $idList, bool $active = true): array;
}
