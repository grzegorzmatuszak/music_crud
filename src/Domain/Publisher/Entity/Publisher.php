<?php
declare(strict_types=1);

namespace App\Domain\Publisher\Entity;

use App\Domain\Publisher\Entity\VO\PublisherAddressVO;
use App\Domain\Publisher\Entity\VO\PublisherIdVO;
use App\Domain\Publisher\Entity\VO\PublisherNameVO;
use App\Domain\Publisher\Entity\VO\PublisherNipVO;
use App\Domain\Song\Entity\Song;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Publisher
{
    private ?PublisherIdVO $id = null;

    private ?PublisherNameVO $name = null;

    private ?PublisherNipVO $nip = null;

    private ?PublisherAddressVO $address = null;

    private ?Collection $songs;

    private bool $active = true;

    private function __construct(
        PublisherIdVO $id,
        PublisherNameVO $name,
        PublisherNipVO $nip,
        PublisherAddressVO $address
    ) {
        $this->songs = new ArrayCollection();
        $this->id = $id;
        $this->name = $name;
        $this->nip = $nip;
        $this->address = $address;
    }

    public static function create(
        PublisherIdVO $id,
        PublisherNameVO $name,
        PublisherNipVO $nip,
        PublisherAddressVO $address
    ) {
        return new self($id, $name, $nip, $address);
    }

    public function getId(): PublisherIdVO
    {
        return $this->id;
    }

    public function setId(PublisherIdVO $id): void
    {
        $this->id = $id;
    }

    public function getName(): PublisherNameVO
    {
        return $this->name;
    }

    public function setName(PublisherNameVO $name): void
    {
        $this->name = $name;
    }

    public function getNip(): PublisherNipVO
    {
        return $this->nip;
    }

    public function setNip(PublisherNipVO $nip): void
    {
        $this->nip = $nip;
    }

    public function getAddress(): PublisherAddressVO
    {
        return $this->address;
    }

    public function setAddress(PublisherAddressVO $address): void
    {
        $this->address = $address;
    }

    public function getSongs(): Collection
    {
        return $this->songs;
    }

    public function addSong(Song $song): void
    {
        if (!$this->songs->contains($song)) {
            $this->songs->add($song);
        }
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function activate(): void
    {
        $this->active = true;
    }

    public function deactivate(): void
    {
        $this->active = false;
    }
}
