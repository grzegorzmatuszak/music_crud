<?php
declare(strict_types=1);

namespace App\Domain\Publisher\Entity\VO;

use App\Application\VO\UuidVO;

final class PublisherIdVO extends UuidVO
{
    public static function create(?string $id): UuidVO
    {
        return new self($id);
    }
}
