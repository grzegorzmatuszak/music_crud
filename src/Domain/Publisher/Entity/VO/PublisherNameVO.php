<?php
declare(strict_types=1);

namespace App\Domain\Publisher\Entity\VO;

use App\Domain\Publisher\Exception\PublisherInvalidArgumentException;

final class PublisherNameVO
{
    private string $name;

    private function __construct(string $name)
    {
        if (strlen($name) < 1) {
            throw new PublisherInvalidArgumentException('Name requires minimum 2 characters');
        }

        if (strlen($name) > 255) {
            throw new PublisherInvalidArgumentException('Name requires maximum 255 characters');
        }
        $this->name = $name;
    }

    public static function create(string $name): self
    {
        return new self($name);
    }

    public function getName(): string
    {
        return $this->name;
    }
}
