<?php
declare(strict_types=1);

namespace App\Domain\Publisher\Entity\VO;

use App\Domain\Publisher\Exception\PublisherInvalidArgumentException;

final class PublisherNipVO
{
    private string $nip;

    private function __construct(string $nip)
    {
        if (strlen($nip) < 1) {
            throw new PublisherInvalidArgumentException('Nip requires minimum 2 characters');
        }

        if (strlen($nip) > 255) {
            throw new PublisherInvalidArgumentException('Nip requires maximum 255 characters');
        }
        $this->nip = $nip;
    }

    public static function create(string $nip): self
    {
        return new self($nip);
    }

    public function getNip(): string
    {
        return $this->nip;
    }
}
