<?php
declare(strict_types=1);

namespace App\Domain\Publisher\Entity\VO;

use App\Domain\Publisher\Exception\PublisherInvalidArgumentException;

final class PublisherAddressVO
{
    private string $address;

    private function __construct(string $address)
    {
        if (strlen($address) < 1) {
            throw new PublisherInvalidArgumentException('Address requires minimum 2 characters');
        }

        if (strlen($address) > 255) {
            throw new PublisherInvalidArgumentException('Address requires maximum 255 characters');
        }
        $this->address = $address;
    }

    public static function create(string $address): self
    {
        return new self($address);
    }

    public function getAddress(): string
    {
        return $this->address;
    }
}
