<?php

namespace App\Domain\Artist\Repository;

interface CreateArtistRepositoryInterface
{
    public function save(
        string $id,
        string $firstName,
        string $lastName,
        string $nick,
        array $songsIds = []
    ): void;
}
