<?php

namespace App\Domain\Artist\Repository;

use App\Domain\Artist\Entity\Artist;

interface GetArtistRepositoryInterface
{
    public function findById(string $id, bool $active = true): Artist;

    public function findByIds(array $idList, bool $active = true): array;
}
