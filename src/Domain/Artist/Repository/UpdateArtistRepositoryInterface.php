<?php

namespace App\Domain\Artist\Repository;

interface UpdateArtistRepositoryInterface
{
    public function deleteArtist(string $id): void;

    public function update(
        string $id,
        string $firstName,
        string $lastName,
        string $nick,
        array $songsIds = []
    ): void;
}
