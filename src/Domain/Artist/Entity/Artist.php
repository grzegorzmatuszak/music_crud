<?php
declare(strict_types=1);

namespace App\Domain\Artist\Entity;

use App\Domain\Artist\Entity\VO\ArtistIdVO;
use App\Domain\Artist\Entity\VO\ArtistNameVO;
use App\Domain\Artist\Entity\VO\ArtistNickVO;
use App\Domain\Song\Entity\Song;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Artist
{
    private ?ArtistIdVO $id = null;

    private ?ArtistNameVO $name = null;

    private ?ArtistNickVO $nick = null;

    private bool $active = true;

    private Collection $songs;

    private function __construct(ArtistIdVO $id, ArtistNameVO $name, ArtistNickVO $nick)
    {
        $this->songs = new ArrayCollection();
        $this->id = $id;
        $this->nick = $nick;
        $this->name = $name;
    }

    public static function create(ArtistIdVO $id, ArtistNameVO $name, ArtistNickVO $nick)
    {
        return new self($id, $name, $nick);
    }

    public function getId(): ArtistIdVO
    {
        return $this->id;
    }

    public function getName(): ArtistNameVO
    {
        return $this->name;
    }

    public function setName(ArtistNameVO $name): void
    {
        $this->name = $name;
    }

    public function getNick(): ArtistNickVO
    {
        return $this->nick;
    }

    public function setNick(ArtistNickVO $nick): void
    {
        $this->nick = $nick;
    }

    public function getSongs(): Collection
    {
        return $this->songs;
    }

    public function addSong(Song $song): void
    {
        if (!$this->songs->contains($song)) {
            $this->songs->add($song);
        }
    }
    public function removeSong(Song $song): void
    {
        if ($this->songs->contains($song)) {
            $this->songs->removeElement($song);
        }
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function activate(): void
    {
        $this->active = true;
    }

    public function deactivate(): void
    {
        $this->active = false;
    }
}
