<?php
declare(strict_types=1);

namespace App\Domain\Artist\Entity\VO;

use App\Domain\Artist\Exception\ArtistInvalidArgumentException;

final class ArtistNickVO
{
    private string $nick;

    private function __construct(string $nick)
    {
        if (strlen($nick) < 1) {
            throw new ArtistInvalidArgumentException('Nick requires minimum 1 characters');
        }

        if (strlen($nick) > 255) {
            throw new ArtistInvalidArgumentException('Nick requires maximum 255 characters');
        }
        $this->nick = $nick;
    }

    public static function create(string $nick): self
    {
        return new self($nick);
    }

    public function getNick(): string
    {
        return $this->nick;
    }
}
