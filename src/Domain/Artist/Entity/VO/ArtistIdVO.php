<?php
declare(strict_types=1);

namespace App\Domain\Artist\Entity\VO;

use App\Application\VO\UuidVO;

final class ArtistIdVO extends UuidVO
{
    public static function create(?string $id): UuidVO
    {
        return new self($id);
    }
}
