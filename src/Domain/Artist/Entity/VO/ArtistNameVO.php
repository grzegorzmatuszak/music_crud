<?php
declare(strict_types=1);

namespace App\Domain\Artist\Entity\VO;

use App\Domain\Artist\Exception\ArtistInvalidArgumentException;

final class ArtistNameVO
{
    private string $firstName;

    private string $lastName;

    private function __construct(string $firstName, string $lastName)
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
    }

    public static function create(string $firstName, string $lastName): self
    {
        if (strlen($firstName) < 2) {
            throw new ArtistInvalidArgumentException('First name requires minimum 2 characters');
        }

        if (strlen($lastName) < 2) {
            throw new ArtistInvalidArgumentException('Last name requires minimum 2 characters');
        }

        if (strlen($firstName) > 100) {
            throw new ArtistInvalidArgumentException('First name requires maximum 100 characters');
        }

        if (strlen($lastName) > 100) {
            throw new ArtistInvalidArgumentException('Last name requires maximum 100 characters');
        }

        return new self($firstName, $lastName);
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }
}
