<?php
declare(strict_types=1);

namespace App\Domain\Artist\Exception;

use InvalidArgumentException;

class ArtistInvalidArgumentException extends InvalidArgumentException
{
}
