<?php
declare(strict_types=1);

namespace App\Domain\Song\Entity\VO;

use App\Domain\Song\Exception\SongInvalidArgumentException;

final class SongGenreVO
{
    private string $genre;

    private function __construct(string $genre)
    {
        if (strlen($genre) < 1) {
            throw new SongInvalidArgumentException('Genre requires minimum 1 characters');
        }

        if (strlen($genre) > 255) {
            throw new SongInvalidArgumentException('Genre requires maximum 255 characters');
        }
        $this->genre = $genre;
    }

    public static function create(string $genre): self
    {
        return new self($genre);
    }

    public function getGenre(): string
    {
        return $this->genre;
    }
}
