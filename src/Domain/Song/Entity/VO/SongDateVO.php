<?php
declare(strict_types=1);

namespace App\Domain\Song\Entity\VO;

use App\Domain\Song\Exception\SongInvalidArgumentException;

final class SongDateVO
{
    private int $year;

    private function __construct(int $year)
    {
        if ($year < 1000 || $year > 9999) {
            throw new SongInvalidArgumentException('Last name requires 4 characters');
        }
        $this->year = $year;
    }

    public static function create(int $year): self
    {
        return new self($year);
    }

    public function getYear(): int
    {
        return $this->year;
    }
}
