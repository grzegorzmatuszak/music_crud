<?php
declare(strict_types=1);

namespace App\Domain\Song\Entity\VO;

use App\Application\VO\UuidVO;

final class SongIdVO extends UuidVO
{
    public static function create(?string $id): UuidVO
    {
        return new self($id);
    }
}
