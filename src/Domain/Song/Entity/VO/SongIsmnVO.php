<?php
declare(strict_types=1);

namespace App\Domain\Song\Entity\VO;

use App\Domain\Song\Exception\SongInvalidArgumentException;

final class SongIsmnVO
{
    private string $ismn;

    private function __construct(string $ismn)
    {
        if (strlen($ismn) < 1) {
            throw new SongInvalidArgumentException('Last name requires minimum 1 characters');
        }

        if (strlen($ismn) > 255) {
            throw new SongInvalidArgumentException('ismn requires maximum 255 characters');
        }
        $this->ismn = $ismn;
    }

    public static function create(string $ismn): self
    {
        return new self($ismn);
    }

    public function getIsmn(): string
    {
        return $this->ismn;
    }
}
