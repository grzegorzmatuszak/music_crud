<?php
declare(strict_types=1);

namespace App\Domain\Song\Entity\VO;

use App\Domain\Song\Exception\SongInvalidArgumentException;

final class SongTitleVO
{
    private string $title;

    private function __construct(string $title)
    {
        if (strlen($title) < 1) {
            throw new SongInvalidArgumentException('Title requires minimum 1 characters');
        }

        if (strlen($title) > 255) {
            throw new SongInvalidArgumentException('Title requires maximum 255 characters');
        }
        $this->title = $title;
    }

    public static function create(string $title): self
    {
        return new self($title);
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
