<?php
declare(strict_types=1);

namespace App\Domain\Song\Entity;

use App\Domain\Artist\Entity\Artist;
use App\Domain\Publisher\Entity\Publisher;
use App\Domain\Song\Entity\VO\SongDateVO;
use App\Domain\Song\Entity\VO\SongGenreVO;
use App\Domain\Song\Entity\VO\SongIdVO;
use App\Domain\Song\Entity\VO\SongIsmnVO;
use App\Domain\Song\Entity\VO\SongTitleVO;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Song
{
    private ?SongIdVO $id = null;

    private ?SongTitleVO $title = null;

    private ?SongGenreVO $genre = null;

    private ?SongIsmnVO $ismn = null;

    private ?SongDateVO $date = null;

    private bool $active = true;

    private Publisher $publisher;

    private Collection $artists;

    private function __construct(
        SongIdVO $id,
        SongTitleVO $title,
        SongGenreVO $genre,
        SongIsmnVO $ismn,
        SongDateVO $date,
        Publisher $publisher
    ) {
        $this->artists = new ArrayCollection();
        $this->id = $id;
        $this->title = $title;
        $this->genre = $genre;
        $this->ismn = $ismn;
        $this->date = $date;
        $this->publisher = $publisher;
    }

    public static function create(
        SongIdVO $id,
        SongTitleVO $title,
        SongGenreVO $genre,
        SongIsmnVO $ismn,
        SongDateVO $date,
        Publisher $publisher
    ) {
        return new self($id, $title, $genre, $ismn, $date, $publisher);
    }

    public function getId(): SongIdVO
    {
        return $this->id;
    }

    public function setId(SongIdVO $id): void
    {
        $this->id = $id;
    }

    public function getTitle(): SongTitleVO
    {
        return $this->title;
    }

    public function setTitle(SongTitleVO $title): void
    {
        $this->title = $title;
    }

    public function getGenre(): SongGenreVO
    {
        return $this->genre;
    }

    public function setGenre(SongGenreVO $genre): void
    {
        $this->genre = $genre;
    }

    public function getIsmn(): SongIsmnVO
    {
        return $this->ismn;
    }

    public function setIsmn(SongIsmnVO $ismn): void
    {
        $this->ismn = $ismn;
    }

    public function getDate(): SongDateVO
    {
        return $this->date;
    }

    public function setDate(SongDateVO $date): void
    {
        $this->date = $date;
    }

    public function getPublisher(): Publisher
    {
        return $this->publisher;
    }

    public function setPublisher(Publisher $publisher): void
    {
        $this->publisher = $publisher;
    }

    public function getArtists(): Collection
    {
        return $this->artists;
    }

    public function getArtist(string $id): ?Artist
    {
        return $this->artists->get($id);
    }

    public function addArtist(Artist $artist): void
    {
        if (!$this->artists->contains($artist)) {
            $this->artists->add($artist);
        }
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function activate(): void
    {
        $this->active = true;
    }

    public function deactivate(): void
    {
        $this->active = false;
    }
}
