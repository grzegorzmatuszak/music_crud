<?php
declare(strict_types=1);

namespace App\Domain\Song\Repository;

use App\Domain\Song\Entity\Song;

interface GetSongRepositoryInterface
{
    public function findById(string $id, bool $active = true): Song;

    public function findByIds(array $idList, bool $active = true): array;
}
