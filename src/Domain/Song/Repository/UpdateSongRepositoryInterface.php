<?php
declare(strict_types=1);

namespace App\Domain\Song\Repository;

interface UpdateSongRepositoryInterface
{
    public function deleteSong(string $id): void;

    public function update(
        string $id,
        string $title,
        string $genre,
        string $ismn,
        int $year,
        string $publisherId,
        array $artistsIds = []
    ): void;
}
