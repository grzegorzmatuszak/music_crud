<?php
declare(strict_types=1);

namespace App\Domain\Song\Repository;

interface CreateSongRepositoryInterface
{
    public function save(
        string $id,
        string $title,
        string $genre,
        string $ismn,
        int $year,
        string $publisherId,
        array $artistsIds = []
    ): void;
}
