<?php
declare(strict_types=1);

namespace App\Domain\Song\Exception;

use InvalidArgumentException;

class SongInvalidArgumentException extends InvalidArgumentException
{
}
