init:
	test -f .env || cp .env.dist .env
	test -f docker-compose.yaml || cp docker-compose.yaml.dist docker-compose.yaml
	test -f phpunit.xml || cp phpunit.xml.dist phpunit.xml
	test -f behat.yml || cp behat.yml.dist behat.yml
	docker-compose up -d
	docker-compose run --rm php-fpm composer install
	docker-compose run --rm php-fpm bin/console doctrine:database:drop --force --if-exists
	docker-compose run --rm php-fpm bin/console doctrine:database:create
	docker-compose run --rm php-fpm php bin/console doctrine:m:m --no-interaction
	docker-compose run --rm php-fpm bin/console doctrine:schema:update --force
	docker-compose run --rm php-fpm bin/console doctrine:fixtures:load --no-interaction

start:
	docker-compose up -d
	docker-compose run --rm php-fpm composer install

unit:
	docker-compose run --rm php-fpm php bin/phpunit

behat:
	docker-compose run --rm php-fpm composer run-script prepare
	docker-compose run --rm php-fpm composer run-script behat

dphp:
	docker exec -it pp_backend_php-fpm_1 bash
